package tool.network;

public class ServerMessage extends Message
{
    private int client;

    public ServerMessage(int client, String msg)
    {
        super(msg);
        this.client = client;
    }

    public int getClient()
    {
        return client;
    }
}
