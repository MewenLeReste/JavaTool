package tool.network;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.*;

public class Server implements Runnable
{
    private int connected = 0;
    private int idx = 0;
    private List<Integer> freeIdx = new ArrayList<>();
    private Handler handler;
    private int port;
    private Thread thread;
    private Map<Integer, StringBuffer> buffers = new HashMap<>();
    private Map<Integer, ServerChannelHandler> handlers = new HashMap<>();
    private Channel channel;
    private String[] localIp = null;

    public Server(Handler handler, int port)
    {
        this.handler = handler;
        this.port = port;
        thread = new Thread(this);
    }

    @SuppressWarnings("EmptyCatchBlock")
	public String[] getIp()
	{
		if (localIp != null)
			return localIp;
		List<String> ips = new ArrayList<>();
		try
		{
			Enumeration e = NetworkInterface.getNetworkInterfaces();
			while (e.hasMoreElements())
			{
				NetworkInterface n = (NetworkInterface) e.nextElement();
				Enumeration ee = n.getInetAddresses();
				while (ee.hasMoreElements())
				{
					InetAddress i = (InetAddress) ee.nextElement();
					ips.add(i.getHostAddress());
				}
			}
		} catch (SocketException e) {}
		localIp = ips.toArray(new String[ips.size()]);
		return localIp;
	}

	public int getPort()
	{
		return port;
	}

	public void start()
    {
        thread.start();
    }

    @SuppressWarnings("EmptyCatchBlock")
    public void stop()
    {
        try
        {
            Collection<ServerChannelHandler> values = handlers.values();
            for (ServerChannelHandler handler : values)
                handler.close();
            channel.close();
            thread.join();
        } catch (Exception e) {}
    }

    @Override
    public void run()
    {
        try
        {
            ServerBootstrap bootstrap = new ServerBootstrap();
            EventLoopGroup parent = new NioEventLoopGroup();
            EventLoopGroup child = new NioEventLoopGroup();
            bootstrap.group(parent, child);
            bootstrap.channel(NioServerSocketChannel.class);
            bootstrap.childHandler(new ServerChannelInitializer(this));
            bootstrap.option(ChannelOption.SO_BACKLOG, 128);
            bootstrap.childOption(ChannelOption.SO_KEEPALIVE, true);
            connected = 1;
            ChannelFuture future = bootstrap.bind(port).sync();
            channel = future.channel();
            channel.closeFuture().sync();
            child.shutdownGracefully();
            parent.shutdownGracefully();
        } catch (Exception e)
        {
            connected = -1;
        }
    }

    synchronized int getIdx()
    {
        if (freeIdx.isEmpty())
            return idx++;
        return freeIdx.remove(0);
    }

    private synchronized int getConnected()
    {
        return connected;
    }

    @SuppressWarnings("all")
    public boolean awaitConnection()
    {
        int connected = getConnected();
        while (connected == 0)
            connected = getConnected();
        return (connected == 1);
    }

    private void sendToAll(String msg)
	{
		Collection<ServerChannelHandler> serverChannelHandlers = handlers.values();
		for (ServerChannelHandler handler : serverChannelHandlers)
			handler.send(msg);
	}

	private void sendToOne(int client, String msg)
	{
		ServerChannelHandler handler = handlers.get(client);
		if (handler != null)
			handler.send(msg);
	}

    public void send(Message msg)
    {
        if (msg instanceof ServerMessage)
        {
            ServerMessage serverMessage = (ServerMessage)msg;
            sendToOne(serverMessage.getClient(), serverMessage.getMsg());
        }
        else
            sendToAll(msg.getMsg());
    }

	synchronized void connect(int client, ServerChannelHandler handler)
    {
        handlers.put(client, handler);
    }

    synchronized void disconnect(int client)
    {
        freeIdx.add(client);
        handlers.remove(client);
        handler.disconnect(client);
    }

    @SuppressWarnings("EmptyCatchBlock")
    void receive(int client, String msg)
    {
        try
        {
            if (!buffers.containsKey(client))
                buffers.put(client, new StringBuffer());
            StringBuffer buffer = buffers.get(client);
            buffer.append(msg);
            ServerMessage message = new ServerMessage(client, buffer.toString());
            if (handler.receive(message))
                buffers.put(client, new StringBuffer());
        } catch (Exception e) {}
    }
}
