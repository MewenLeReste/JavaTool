package tool.network;

public interface Handler
{
    boolean receive(Message msg);
    void disconnect(int idx);
}
