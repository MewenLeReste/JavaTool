package tool.network;

public class ServerChannelHandler extends ANetworkChannelHandler
{
    private int idx;
    private Server server;

    ServerChannelHandler(Server server)
    {
        this.server = server;
        idx = server.getIdx();
    }

    @Override
    public void connect()
    {
        server.connect(idx, this);
    }

    @Override
    public void disconnect()
    {
        server.disconnect(idx);
    }

    @Override
    public void read(String msg)
    {
        server.receive(idx, msg);
    }
}