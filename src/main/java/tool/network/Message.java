package tool.network;

public class Message
{
    private String msg;

    public Message(String msg)
    {
        this.msg = msg;
    }

    public String getMsg()
    {
        return msg;
    }
}
