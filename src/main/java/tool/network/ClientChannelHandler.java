package tool.network;

public class ClientChannelHandler extends ANetworkChannelHandler
{
    private Client client;

    public ClientChannelHandler(Client client)
    {
        this.client = client;
    }

    @Override
    void connect()
    {
        client.connect();
    }

    @Override
    void disconnect()
    {
        client.disconnect();
    }

    @Override
    void read(String msg)
    {
        client.read(msg);
    }
}
