package tool.network;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import tool.Host;

public class Client implements Runnable
{
    private StringBuffer buffer = new StringBuffer();
    private Handler handler;
    private EventLoopGroup workerGroup = new NioEventLoopGroup();
    private ClientChannelHandler channelHandler = new ClientChannelHandler(this);
    private int connected = 0;
    private Host hostname;
    private Thread thread;

    public Client(Handler handler, Host hostname)
    {
        this.handler = handler;
        this.hostname = hostname;
        thread = new Thread(this);
    }

    public void start()
    {
        thread.start();
    }

    @SuppressWarnings("EmptyCatchBlock")
    public void stop()
    {
        channelHandler.close();
        try
        {
            thread.interrupt();
            thread.join();
        } catch (Exception e) {}
    }

    @Override
    public void run()
    {
        try
        {
            String host = hostname.getIp();
            int port = hostname.getPort();
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(workerGroup);
            bootstrap.channel(NioSocketChannel.class);
            bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
            bootstrap.handler(new ChannelInitializer<SocketChannel>()
            {
                @Override
                public void initChannel(SocketChannel ch)
                {
                    ch.pipeline().addLast(channelHandler);
                }
            });
            ChannelFuture future = bootstrap.connect(host, port).sync();
            future.channel().closeFuture().addListener((ChannelFuture cf) -> disconnect()).sync();
            workerGroup.shutdownGracefully().sync();
        } catch (Exception e)
        {
            connected = -1;
        }
    }

    synchronized void connect()
    {
        connected = 1;
    }

    public void send(Message msg)
    {
        channelHandler.send(msg.getMsg());
    }

    private synchronized int getConnected()
    {
        return connected;
    }

    @SuppressWarnings("all")
    public boolean awaitConnection()
    {
        int connected = getConnected();
        while (connected == 0)
            connected = getConnected();
        return (connected == 1);
    }

    @SuppressWarnings("EmptyCatchBlock")
    synchronized void read(String msg)
    {
        try
        {
            buffer.append(msg);
            Message message = new Message(buffer.toString());
            if (handler.receive(message))
                buffer = new StringBuffer();
        } catch (Exception e) {}
    }

    synchronized void disconnect()
    {
        if (connected == 1)
            handler.disconnect(-1);
        connected = -1;
    }
}
