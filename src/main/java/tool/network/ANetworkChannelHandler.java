package tool.network;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

@SuppressWarnings("EmptyCatchBlock")
public abstract class ANetworkChannelHandler extends ChannelInboundHandlerAdapter
{
    private ChannelHandlerContext ctx = null;

    void close()
    {
        if (ctx != null)
        {
            ctx.close();
            ctx = null;
        }
    }

    void send(String msg)
    {
        try
        {
            msg += '\n';
            ByteBuf out = ctx.alloc().buffer(msg.length() + 2);
            out.writeBytes(msg.getBytes(StandardCharsets.UTF_8));
            if (ctx != null)
                ctx.writeAndFlush(out);
        } catch (Exception e) {}
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx)
    {
        try
        {
            this.ctx = ctx;
            connect();
        } catch (Exception e) {}
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx)
    {
        try
        {
            this.ctx = ctx;
            ctx.flush();
        } catch (Exception e) {}
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx)
    {
        try
        {
            disconnect();
            if (this.ctx != null)
            {
                this.ctx.close();
                this.ctx = null;
            }
            ctx.fireChannelInactive();
        } catch (Exception e) {}
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg)
    {
        try
        {
            this.ctx = ctx;
            ByteBuf buffer = (ByteBuf) msg;
            String content = buffer.toString(CharsetUtil.UTF_8);
            read(content);
            buffer.release();
        } catch (Exception e) {}
    }

    abstract void connect();
    abstract void disconnect();
    abstract void read(String msg);
}
