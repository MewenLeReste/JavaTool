package tool.network;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

public class ServerChannelInitializer extends ChannelInitializer<SocketChannel>
{
    private Server server;

    public ServerChannelInitializer(Server server)
    {
        this.server = server;
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception
    {
        ch.pipeline().addLast(new ServerChannelHandler(server));
    }
}
