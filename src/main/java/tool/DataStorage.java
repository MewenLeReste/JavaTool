package tool;

import tool.encryption.TCipher;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class DataStorage
{
    private class Segment
    {
        String value = "";
        private HashMap<String, Segment> segments = new HashMap<>();

        private Pair<String, String> getSegmentKey(String key)
        {
            int indexOf = key.indexOf(':');
            String newSegment;
            if (indexOf != -1)
                newSegment = key.substring(0, indexOf);
            else
                newSegment = key;
            String newKey;
            if (indexOf != -1)
                newKey = key.substring(indexOf + 1);
            else
                newKey = "";
            return new Pair<>(newSegment, newKey);
        }

        private List<String> appendRemoveList(String segmentName, List<String> removedList)
        {
            ArrayList<String> ret = new ArrayList<>();
            for (String removedSegment : removedList)
                ret.add("$segmentName:$removedSegment");
            return ret;
        }

        List<String> remove(String key)
        {
            Pair<String, String> pair = getSegmentKey(key);
            String newSegment = pair.first;
            String newKey = pair.second;
            if (newKey.isEmpty())
            {
                List<String> list = new ArrayList<>();
                Segment segment = segments.remove(key);
                if (segment != null)
                    list = segment.toList();
                return appendRemoveList(newSegment, list);
            }
            Segment segment = segments.get(newSegment);
            if (segment == null)
                return new ArrayList<>();
            return appendRemoveList(newSegment, segment.remove(newKey));
        }

        Segment getSegment(String key)
        {
            Pair<String, String> pair = getSegmentKey(key);
            String newSegment = pair.first;
            String newKey = pair.second;
            Segment segment = segments.get(newSegment);
            if (segment == null)
                return null;
            if (!newKey.isEmpty())
                return segment.getSegment(newKey);
            else
                return segment;
        }

        String get(String key)
        {
            Segment segment = getSegment(key);
            if (segment == null)
                return "";
            return segment.value;
        }

        boolean isEmpty()
        {
            return segments.isEmpty();
        }

        boolean isNotEmpty()
        {
            return !segments.isEmpty();
        }

        void clear()
        {
            value = "";
            segments.clear();
        }

        void newValue(String key, String value)
        {
            Pair<String, String> pair = getSegmentKey(key);
            String newSegment = pair.first;
            String newKey = pair.second;
            if (!this.segments.containsKey(newSegment))
                segments.put(newSegment, new Segment());
            Segment createdSegments = this.segments.get(newSegment);
            if (!newKey.isEmpty())
                createdSegments.newValue(newKey, value);
            else
                createdSegments.value = value;
        }

        List<String> toList()
        {
            List<String> ret = new ArrayList<>();
            Set<Map.Entry<String, Segment>> entries = segments.entrySet();
            for (Map.Entry<String, Segment> segment : entries)
            {
                List<String> segmentList = segment.getValue().toList();
                String key = segment.getKey();
                if (!segmentList.isEmpty())
                {
                    for (String subSegment : segmentList)
                        ret.add(key + ":" + subSegment);
                }
                ret.add(key);
            }
            return ret;
        }
    }

    private boolean autoSave;
    private TCipher cipher;
    private String dbPath;
    private Segment segments = new Segment();
    private Map<String, Set<String>> keyDB = new HashMap<>();

    public DataStorage(String path)
    {
        init(path, "", false, false);
    }

    public DataStorage(String path, String encryptionKey)
    {
        init(path, encryptionKey, false, false);
    }

    public DataStorage(String path, String encryptionKey, boolean autoLoad, boolean autoSave)
    {
        init(path, encryptionKey, autoLoad, autoSave);
    }

    public DataStorage(String path, boolean autoLoad, boolean autoSave)
    {
        init(path, "", autoLoad, autoSave);
    }

    private void init(String path, String encryptionKey, boolean autoLoad, boolean autoSave)
    {
        dbPath = path;
        this.autoSave = autoSave;
        if (encryptionKey.length() >= 16)
        {
            try
            {
                cipher = new TCipher(encryptionKey.substring(0, 16));
            } catch (Exception e)
            {
                cipher = null;
            }
        }
        else
            cipher = null;
        if (autoLoad)
            load();
    }

    public void clear()
    {
        segments.clear();
        keyDB.clear();
    }

    public List<String> remove(String key)
    {
        return segments.remove(key);
    }

    public void set(String key, String value)
    {
        if (value.contains("\n") || key.contains("="))
            return;
        segments.newValue(key, value);
        if (autoSave)
            save();
    }

    public void set(List<String> keys, List<String> values)
    {
        for (String value : values)
        {
            if (value.contains("\n"))
                return;
        }
        for (String key : keys)
        {
            if (key.contains("="))
                return;
        }
        for (String key : keys)
        {
            if (!keyDB.containsKey(key))
                keyDB.put(key, new HashSet<>());
            Set<String> set = keyDB.get(key);
            set.addAll(values);
        }
        if (autoSave)
            save();
    }

    public List<String> getSegments(String key)
    {
        if (key.isEmpty())
            return segments.toList();
        else
        {
            Segment segment = segments.getSegment(key);
            if (segment != null)
                return segment.toList();
            else
                return new ArrayList<>();
        }
    }

    public String get(String key)
    {
        return segments.get(key);
    }

    public List<String> get(List<String> keys)
    {
        List<String> ret = new ArrayList<>();
        for (String key : keys)
            ret.add(segments.get(key));
        return ret;
    }

    private String loadDbFile()
    {
        if (cipher != null)
        {
            try
            {
                return cipher.loadEncryptedFile(dbPath);
            } catch (Exception e)
            {
                return "";
            }
        }
        else
        {
            try
            {
                File inputFile = new File(dbPath);
                FileInputStream inputStream = new FileInputStream(inputFile);
                byte[] inputBytes = new byte[(int)inputFile.length()];
                if (inputStream.read(inputBytes) == 0)
                    return "";
                return new String(inputBytes, StandardCharsets.ISO_8859_1);
            } catch (Exception e)
            {
                return "";
            }
        }
    }

    public void load()
    {
        File file = new File(dbPath);
        if (file.exists() && file.isFile())
        {
            boolean inKey = false;
            String currentKey = "";
            String content = loadDbFile();
            String[] lines = content.split(System.getProperty("line.separator"));
            for (String line : lines)
            {
                if (inKey)
                {
                    if (line.equals("]"))
                        inKey = false;
                    else
                    {
                        if (!keyDB.containsKey(currentKey))
                            keyDB.put(currentKey, new HashSet<>());
                        Set<String> set = keyDB.get(currentKey);
                        set.add(line);
                    }

                }
                else if (line.endsWith("=["))
                {
                    currentKey = line.substring(0, line.length() - 2);
                    inKey = true;
                }
                else if (line.contains("="))
                {
                    segments.newValue(line.substring(0, line.indexOf("=")), line.substring(line.indexOf("=") + 1));
                }
            }
        }
    }

    public void save()
    {
        new File(dbPath).delete();
        String content = toString();
        if (content.isEmpty())
            return;
        if (cipher != null)
        {
            try
            {
                cipher.saveEncryptedFile(dbPath, content);
            } catch (Exception e) {}
        }
        else
        {
            try
            {
                FileOutputStream outputStream = new FileOutputStream(dbPath);
                outputStream.write(content.getBytes());
                outputStream.close();
            } catch (Exception e) {}
        }
    }

    @Override
    public String toString()
    {
        if (segments.isEmpty() && keyDB.isEmpty())
            return "";
        StringBuilder fileBuilder = new StringBuilder();
        List<String> segmentKeys = segments.toList();
        for (String key : segmentKeys)
        {
            String value = segments.get(key);
            if (!value.isEmpty())
            {
                fileBuilder.append(key);
                fileBuilder.append('=');
                fileBuilder.append(value);
                fileBuilder.append('\n');
            }
        }
        Set<Map.Entry<String, Set<String>>> entries = keyDB.entrySet();
        for (Map.Entry<String, Set<String>> value : entries)
        {
            fileBuilder.append(value.getKey());
            fileBuilder.append("=[\n");
            for (String element : value.getValue())
            {
                fileBuilder.append(element);
                fileBuilder.append('\n');
            }
            fileBuilder.append("]\n");
        }
        String ret = fileBuilder.toString();
        return ret.substring(0, ret.length() - 1);
    }
}