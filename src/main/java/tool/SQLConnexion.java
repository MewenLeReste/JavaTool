package tool;

import java.io.Closeable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * A class that allow the user to connect to an SQL base and ask request to the database
 */
@SuppressWarnings("unused")
public final class SQLConnexion implements Closeable
{
	private Connection connexion = null;
	private Statement statement = null;
	private boolean connected;

	/**
	 * Initialize the SQL connexion with:
	 * <br>- The database URL
	 * <br>- The user name
	 * <br>- The user password
	 * 
	 * @param host Database {@link Host}
	 * @param user User name
	 * @param password User password
	 */
	public SQLConnexion(Host host, String user, String password)
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			connexion = DriverManager.getConnection("jdbc:mysql://" + host, user, password);
			statement = connexion.createStatement();
			connected = true;
		} catch (Exception e)
		{
			connected = false;
			e.printStackTrace();
		}
	}

	/**
	 * Specify if {@link SQLConnexion} was able to connect to SQL database
	 *
	 * @return true if the {@link SQLConnexion} is connected
	 */
	public boolean isConnected()
	{
		return connected;
	}

	/**
	 * Execute an SQL Update. For Example
	 * <br>INSERT
	 * 
	 * @param request Update to execute
	 * @return -1 if the request fail
	 */
	public int executeUpdate(String request)
	{
		int result;
		try
		{
			result = statement.executeUpdate(request);
		} catch (Exception e)
		{
            e.printStackTrace();
			return -1;
		}
		return result;
	}

	/**
	 * Execute an SQL Query. For Example
	 * <br>SELECT
	 * 
	 * @param request Query to execute
	 * @return Result of the Query
	 */
	public ResultSet executeQuery(String request)
	{
		ResultSet result;
		try
		{
			result = statement.executeQuery(request);
		} catch (Exception e)
		{
            e.printStackTrace();
			return null;
		}
		return result;
	}

	/**
	 * Close the SQLConnection
	 */
	public void close()
	{
		if (connexion != null)
		{
			try
			{
				connexion.close();
			} catch (Exception e)
			{
                e.printStackTrace();
			}
		}
	}
}
