package tool;

import java.awt.Dimension;
import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.poi.util.IOUtils;

/**
 * The only known image format for tool
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class TImage
{
	private Dimension size;
	private String path;
	private String suffix;
	private File file;
	protected boolean created;
	private byte[] bytes;

	/**
	 * Copy an other {@link TImage} in this one
	 * 
	 * @param image {@link TImage} to copy
	 */
	public TImage(TImage image)
	{
		path = image.getPath();
		file = image.getFile();
		bytes = image.getBytes();
		size = image.getSize();
		suffix = image.getSuffix();
		created = image.isCreated();
	}
	
	/**
	 * Load the image at the given path
	 * 
	 * @param path Path of the image to load
	 */
	public TImage(String path)
	{
		try
		{
			this.path = path;
			file = new File(path);
			Image img = ImageIO.read(file);
			InputStream input = new FileInputStream(file);
			bytes = IOUtils.toByteArray(input);
			input.close();
			size = new Dimension(img.getWidth(null), img.getHeight(null));
			suffix = path.substring(path.lastIndexOf(".") + 1);
			created = true;
		} catch (Exception e)
		{
		    e.printStackTrace();
			created = false;
		}
	}
	
	@Override
	public String toString()
	{
		return "Image [path=" + path + ", created=" + created + ", size=" + size.getWidth() + "*" + size.getHeight() + ", type=" + suffix + "]";
	}

	/**
	 * Precise if the image has been created or not
	 * 
	 * @return true if the image has been created
	 */
	public final boolean isCreated()
	{
		return created;
	}

	/**
	 * Return the type of the image
	 * 
	 * @return The type of the image
	 */
	public final File getFile()
	{
		if (created)
			return file;
		return null;
	}

	/**
	 * Return the path of the image
	 * 
	 * @return The path of the image
	 */
	public final String getPath()
	{
		if (created)
			return path;
		return null;
	}

	/**
	 * Return the type of the image
	 * 
	 * @return The type of the image
	 */
	public final String getSuffix()
	{
		if (created)
			return suffix;
		return null;
	}
	
	/**
	 * Return the size of the image
	 * 
	 * @return The size of the image
	 */
	public final Dimension getSize()
	{
		if (created)
			return size;
		return null;
	}
	
	/**
	 * Return the bytes composing the image
	 * 
	 * @return The bytes composing the image
	 */
	public final byte[] getBytes()
	{
		if (created)
			return bytes;
		return null;
	}
	
	/**
	 * Delete the image
	 * 
	 * @return true if the image has been deleted
	 */
	public final boolean delete()
	{
		if (created)
			return file.delete();
		return false;
	}
}
