package tool.shell;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import tool.Time;

/**
 * Execute simple Shell command
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class Shell
{
	public static final Time NOLIMIT = null;
	private static Time LIMIT = NOLIMIT;
	
	/**
	 * Create a file with the given content 
	 * 
	 * @param path Path where to create the file
	 * @param content Content of the file
	 * @return true if the file has been correctly created
	 */
	public static boolean createFile(String path, String content)
	{
		try
		{
			FileWriter file = new FileWriter(new File(path));
			file.write(content);
			file.close();
		} catch (Exception e)
		{
			return false;
		}
		return true;
	}

	/**
	 * Set the {@link Shell#execute} time limit to the given one
	 * 
	 * @param newLimit {@link Shell#execute} time limt 
	 */
	public static void setLimit(Time newLimit)
	{
		LIMIT = newLimit.getTime();
	}
	
	/**
	 * Execute a shell command with a time limit if set (see {@link Shell#setLimit})
	 * 
	 * @param command Shell command
	 * @return The command standard output or null if the command crash
	 */
	public static ShellOutput execute(String command)
	{
		try
		{
			ShellOutput output = new ShellOutput();
			Time begin = new Time();
			output.setBegin(begin.getTime());
			boolean stop = false;
			Process proc = Runtime.getRuntime().exec(command);
			while (!stop)
			{
				if (!proc.isAlive())
					stop = true;
				else
				{
					if (LIMIT != null)
					{
						Time actual = new Time();
						Time diff = Time.diff(begin, actual);
						if (!diff.isBefore(LIMIT))
						{
							proc.destroyForcibly();
							System.err.println("The processus has been stopped");
							output.setEnd(actual);
							output.setExit(-1);
							output.setError("Stopped");
							output.setStandard("Stopped");
							return output;
						}
					}
				}
			}
			output.setEnd(new Time());
			InputStream error = proc.getErrorStream();
			InputStream normal = proc.getInputStream();
			output.setExit(proc.exitValue());
			String err = IOUtils.toString(error, "UTF-8");
			String print = IOUtils.toString(normal, "UTF-8");
			output.setError(err);
			output.setStandard(print);
            System.err.println(err);
            System.out.println(print);
			return output;
		} catch (Exception error)
		{
		    error.printStackTrace();
			return null;
		}
	}
	
	/**
	 * If necessary rename the file in (X)file_name where X is the number of file with the same name
	 * 
	 * @param name Name of the file
	 * @return The name of the file (change if necesary)
	 */
	public static String getName(String name)
	{
		if (name.isEmpty())
			return null;
		String filePath = name.substring(0, name.lastIndexOf("/") + 1);
		String fileName = name.substring(name.lastIndexOf("/") + 1);
		String file = filePath + fileName;
		if (new File(file).exists())
		{
			int n = 1;
			while (new File(file).exists())
			{
				file = filePath + "(" + n + ")" + fileName;
				n++;
			}
		}
		return file;
	}
	
	/**
	 * Copy/Move a file from oldPath to newPath
	 * 
	 * @param oldPath Path to the file to copy/move
	 * @param newPath Path where to copy/move
	 * @param copy Specify if we copy or not (true for copy, false to move)
	 * @return the new path of the file
	 */
	public static String move(String oldPath, String newPath, boolean copy)
	{
		try
		{
			newPath = getName(newPath);
			if (newPath == null)
				return null;
			BufferedInputStream in = new BufferedInputStream(new FileInputStream(oldPath));
			BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(newPath));
			byte[] buf = new byte[4096];
			int n;
			while ((n = in.read(buf, 0, buf.length)) > 0)
				out.write(buf, 0, n);
			in.close();
			out.close();
			if (!copy)
				new File(oldPath).delete();
			return newPath;
		} catch (Exception error)
		{
		    error.printStackTrace();
			return null;
		}
	}
}
