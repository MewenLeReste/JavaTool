package tool.shell;

import tool.Time;

/**
 * Output of {@link Shell#execute} with:
 * <br>The value of the exit
 * <br>The standard output
 * <br>The error output
 * <br>The {@link Time} of beginning
 * <br>The {@link Time} of ending
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class ShellOutput
{
	private int exit;
	private String standard;
	private String error;
	private Time begin;
	private Time end;

	public Time getBegin()
	{
		return begin;
	}
	public Time getEnd()
	{
		return end;
	}
	public int getExit()
	{
		return exit;
	}
	public String getStandard()
	{
		return standard;
	}
	public String getError()
	{
		return error;
	}

	@Override
	public String toString()
	{
		return "ShellOutput\nExit value: " + exit + "\nDate of begin: " + begin.format(Time.TIME) + "\nDate of end: " + end.format(Time.TIME) + "\nStandard output:\n" + standard + "\nError output:\n" + error;
	}

	public void setBegin(Time begin)
	{
		this.begin = begin;
	}
	public void setEnd(Time end)
	{
		this.end = end;
	}
	public void setExit(int exit)
	{
		this.exit = exit;
	}
	public void setStandard(String standard)
	{
		this.standard = standard;
	}
	public void setError(String error)
	{
		this.error = error;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;
		if (!obj.getClass().equals(ShellOutput.class))
			return false;
		ShellOutput other = (ShellOutput) obj;
		if (!error.equals(other.getError()))
			return false;
		if (exit != other.getExit())
			return false;
		return (standard.equals(other.getStandard()));
	}
}
