/**
 * A set of classes allowing us to manage the following Input/Output:
 * <br>- Output:
 * <br>----- Console
 * <br>----- Shell
 * <br>- Input:
 * <br>----- Command
 * <br>----- Shell
 */
package tool.shell;