package tool.mail;

import org.apache.commons.lang3.RandomStringUtils;
import tool.TImage;

/**
 * Image for the {@link Mail}
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class MailImage
{
	private TImage image;
	private String cid;
	
	public MailImage(TImage image)
	{
		this.image = image;
		this.cid = RandomStringUtils.random(12, true, false);
	}
	
	public MailImage(TImage image, String cid)
	{
		this.image = image;
		this.cid = cid;
	}
	
	public TImage getImage()
	{
		return image;
	}
	
	public String getCID()
	{
		return cid;
	}
}
