package tool.mail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import tool.TImage;
import tool.ms.TDocument;

/**
 * A class allowing you to manage an e-mail
 * <br>Use html for content
 */
@SuppressWarnings({"unused", "WeakerAccess", "UnusedReturnValue"})
public class Mail
{
	private Properties properties;
	private Session session;
	private MimeMessage mail;
	private LinkedList<MailImage> images;
	private LinkedList<Address> recipients;
	private LinkedList<Address> hiddenCopy;
	private LinkedList<Address> copy;
	private LinkedList<File> attachments;
	private String content;
	private String subject;
	private InternetAddress from;

    {
        properties = System.getProperties();
        recipients = new LinkedList<>();
        hiddenCopy = new LinkedList<>();
        copy = new LinkedList<>();
        attachments = new LinkedList<>();
        images = new LinkedList<>();
        content = "<html><body>";
    }

	public Mail() {}
	
	public Mail(String from, String[] to, String subject)
	{
		setFrom(from);
		addRecipients(to);
		setSubject(subject);
	}
	
	public Mail(String from, String to, String subject)
	{
		setFrom(from);
		addRecipients(to);
		setSubject(subject);
	}

	private boolean addToList(String str, List<Address> list)
    {
        try
        {
            if (str == null)
                return false;
            list.add(new InternetAddress(str));
            return true;
        } catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

	/**
	 * Set the recipient of an copy of the email
	 * 
	 * @param cc Recipient of an copy of the email
	 * @return true if the recipient of an copy has been set
	 */
	public boolean addCopyRecipients(String cc)
	{
	    return addToList(cc, copy);
	}
	
	/**
	 * Set the recipients of an copy of the email
	 * 
	 * @param cc Recipients of an copy of the email
	 * @return true if the recipients of an copy has been set
	 */
	public boolean addCopyRecipients(String[] cc)
	{
		if (cc == null)
			return false;
		for (String receiver : cc)
		{
			if (!addRecipients(receiver))
				return false;
		}
		return true;
	}
	
	/**
	 * Set the recipient of an hidden copy of the email
	 * 
	 * @param bcc Recipient of an hidden copy of the email
	 * @return true if the recipient of an hidden copy has been set
	 */
	public boolean addHiddenCopyRecipients(String bcc)
	{
        return addToList(bcc, hiddenCopy);
	}
	
	/**
	 * Set the recipients of an hidden copy of the email
	 * 
	 * @param bcc Recipients of an hidden copy of the email
	 * @return true if the recipients of an hidden copy has been set
	 */
	public boolean addHiddenCopyRecipients(String[] bcc)
	{
		if (bcc == null)
			return false;
		for (String receiver : bcc)
		{
			if (!addRecipients(receiver))
				return false;
		}
		return true;
	}
	
	/**
	 * Set the receiver of the email
	 * 
	 * @param to Receiver of the email
	 * @return true if the receiver has been set
	 */
	public boolean addRecipients(String to)
	{
        return addToList(to, recipients);
	}
	
	/**
	 * Set the receivers of the email
	 * 
	 * @param to Receivers of the email
	 * @return true if the receivers has been set
	 */
	public boolean addRecipients(String[] to)
	{
		if (to == null)
			return false;
		for (String receiver : to)
		{
			if (!addRecipients(receiver))
				return false;
		}
		return true;
	}
	
	/**
	 * Set the sender of the email
	 * 
	 * @param from Sender of the email
	 * @return true if the sender has been set
	 */
	public boolean setFrom(String from)
	{
		try
		{
			if (from == null)
				return false;
			this.from = new InternetAddress(from);
			return true;
		} catch (Exception e)
		{
		    e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Set the subject of the email
	 * 
	 * @param subject Subject of the email
	 * @return true if the subject has been set
	 */
	public boolean setSubject(String subject)
	{
		if (subject == null)
			return false;
		this.subject = subject;
		return true;
	}
	
	/**
	 * Add an attachment file to the email
	 * 
	 * @param attachment File to add
	 * @return true if the attachment has been added
	 */
	public boolean addAttachment(File attachment)
	{
		if (attachment == null || !attachment.exists())
			return false;
		attachments.add(attachment);
		return true;
	}
	
	/**
	 * Add an attachment file to the email
	 * 
	 * @param document TDocument to add
	 * @return true if the attachment has been added
	 */
	public boolean addAttachment(TDocument<?> document)
	{
		File attachment = document.getFile();
		if (attachment == null || !attachment.exists())
			return false;
		attachments.add(attachment);
		return true;
	}
	
	/**
	 * Add a list of attachments files to the email
	 * 
	 * @param attachments List of files to add
	 * @return true if the attachments has been added
	 */
	public boolean addAttachment(List<File> attachments)
	{
		if (attachments == null)
			return false;
		for (File attachment : attachments)
		{
			if (!addAttachment(attachment))
				return false;
		}
		return true;
	}
	
	/**
	 * Add content from a file to the email
	 * 
	 * @param content File content to add
	 * @return true if the content has been added
	 */
	public boolean addContent(File content)
	{
		if (content == null)
			return false;
		try {
            return addContent(IOUtils.toString(new FileInputStream(content), "UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
	}
	
	/**
	 * Add content to the email
	 * 
	 * @param content Content to add
	 * @return true if the content has been added
	 */
	public boolean addContent(String content)
	{
		if (content == null)
			return false;
		content = content.replaceAll("\n", "<br>");
		this.content += content;
		return true;
	}
	
	/**
	 * Add an image to the email
	 * 
	 * @param content Image to add
	 * @return true if the image has been added
	 */
	public boolean addContent(MailImage content)
	{
		if (content == null || content.getImage() == null || !content.getImage().isCreated())
			return false;
		String cid = content.getCID();
		this.content += "<img src=\"cid:" + cid + "\"/>";
		images.add(content);
		return true;
	}
	
	/**
	 * Add an image to the email
	 * 
	 * @param content Image to add
	 * @return true if the image has been added
	 */
	public boolean addContent(TImage content)
	{
		if (content == null || !content.isCreated())
			return false;
		String cid = RandomStringUtils.random(12, true, false);
		this.content += "<img src=\"cid:" + cid + "\"/>";
		images.add(new MailImage(content, cid));
		return true;
	}
	
	private boolean prepareMail()
	{
		try
		{
			MimeMultipart multipart = new MimeMultipart("related");
			MimeBodyPart body = new MimeBodyPart();
			body.setText(content + "</body></html>", "US-ASCII", "html");
			multipart.addBodyPart(body);
			for (MailImage image : images)
			{
				MimeBodyPart imagePart = new MimeBodyPart();
				imagePart.attachFile(image.getImage().getFile());
				imagePart.setContentID("<" + image.getCID() + ">");
				imagePart.setDisposition(MimeBodyPart.INLINE);
				multipart.addBodyPart(imagePart);
			}
			for (File attachment : attachments)
			{
				MimeBodyPart part = new MimeBodyPart();
				DataSource source = new FileDataSource(attachment);
				part.setDataHandler(new DataHandler(source));
				part.setFileName(attachment.getName());
				multipart.addBodyPart(part);
			}
			session = Session.getDefaultInstance(properties);
			mail = new MimeMessage(session);
			mail.setContent(multipart);
			mail.setFrom(from);
			mail.setSubject(subject);
			Address[] recipientsList = new Address[recipients.size()];
			for (int n = 0; n < recipients.size(); n++)
				recipientsList[n] = recipients.get(n);
			mail.addRecipients(Message.RecipientType.TO, recipientsList);
			Address[] hiddenCopyList = new Address[hiddenCopy.size()];
			for (int n = 0; n < hiddenCopy.size(); n++)
				hiddenCopyList[n] = hiddenCopy.get(n);
			mail.addRecipients(Message.RecipientType.BCC, hiddenCopyList);
			Address[] copyList = new Address[copy.size()];
			for (int n = 0; n < copy.size(); n++)
				copyList[n] = copy.get(n);
			mail.addRecipients(Message.RecipientType.CC, copyList);
			return true;
		} catch (Exception e)
		{
		    e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Save the email in the given file
	 * 
	 * @param path Path of the File
	 * @return true if the email has been saved
	 */
	public boolean saveAs(String path)
	{
		try
		{
			if (path == null)
				return false;
			prepareMail();
			mail.writeTo(new FileOutputStream(new File(path)));
			return true;
		} catch (Exception e)
		{
		    e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Set the email properties to the given one
	 * 
	 * @param properties New properties of the email
	 * @return true if the properties has been set
	 */
	public boolean setProperties(Properties properties)
	{
		if (properties == null)
			return false;
		this.properties = properties;
		return true;
	}

	/**
	 * Add a properties to the email
	 * 
	 * @param properties Properties to add
	 * @param value Value of the properties to add
	 * @return true if the properties has been added
	 */
	public boolean addProperties(String properties, String value)
	{
		if (properties == null || value == null)
			return false;
		this.properties.put(properties, value);
		return true;
	}
	
	/**
	 * Sent the email using the given protocol, login and password
	 * 
	 * @param protocol Protocol to use to send the email
	 * @param host of the email
	 * @param login Login to use to send the email
	 * @param password Password to use to send the email
	 * @return true if the email has been sent
	 */
	public boolean send(String protocol, String host, String login, String password)
	{
		try
		{
			if (protocol == null || host == null || login == null || password == null || recipients.size() <= 0)
				return false;
			prepareMail();
			Transport transport = session.getTransport(protocol);
			transport.connect(host, login, password);
			transport.sendMessage(mail, mail.getAllRecipients());
			transport.close();
			return true;
		} catch (Exception e)
		{
		    e.printStackTrace();
			return false;
		}
	}
}
