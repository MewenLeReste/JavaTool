package tool;

@SuppressWarnings("unused")
public class Host
{
    private String ip;
    private int port;

    public Host(String ip)
    {
        String[] arr = ip.split(":");
        this.ip = arr[0];
        if (arr.length > 1)
            port = Integer.parseInt(arr[1]);
        else
            port = -1;
    }

    public Host(String ip, int port)
    {
        this.ip = ip;
        this.port = port;
    }

    public Host(int port)
    {
        ip = "localhost";
        this.port = port;
    }

    public String getIp()
    {
        return ip;
    }

    public int getPort()
    {
        return port;
    }

    @Override
    public String toString()
    {
        if (port < 0)
            return ip;
        return ip + ':' + port;
    }
}
