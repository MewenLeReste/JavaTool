package tool;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

@SuppressWarnings({"unused", "unchecked", "WeakerAccess"})
public class Json
{
    private JSONObject data;

    /**
     * Load a Json from a file
     *
     * @param path Path to the file
     * @return A {@link Json} loading from a file
     */
    public static Json loadFromFile(String path)
    {
        try
        {
            return new Json(new FileInputStream(path));
        } catch (Exception e)
        {
            return null;
        }
    }

    /**
     * Load multiple jsons from a concatenated jsons string
     *
     * @param content String containing all concatenated jsons
     * @return A {@link List} of {@link Json} loading from a concatenated string
     */
    public static List<Json> loadFromConcatenatedContent(String content)
    {
        List<Json> jsons = new ArrayList();
        String[] strings = content.trim().split("}\\s*\\{");
        if (strings.length == 0)
            return jsons;
        if (strings.length == 1)
        {
            jsons.add(new Json(content));
            return jsons;
        }
        int idx = 0;
        for (String jsonContent : strings)
        {
            Json json;
            if (idx == 0)
                json = new Json(jsonContent + "}");
            else if (idx == strings.length - 1)
                json = new Json("{" + jsonContent);
            else
                json = new Json("{" + jsonContent + "}");
            jsons.add(json);
            ++idx;
        }
        return jsons;
    }

    /**
     * Copy the given {@link Json} to a new one
     *
     * @param from {@link Json} to copy
     * @return A copy of the given {@link Json}
     */
    public static Json copy(Json from)
    {
        return new Json(from.data);
    }

    /**
     * Create an empty Json
     */
    public Json()
    {
        data = new JSONObject();
    }

    /**
     * Create a Json with the given {@link JSONObject}
     *
     * @param value Already created {@link JSONObject}
     */
    public Json(JSONObject value)
    {
        data = value;
    }

    /**
     * Create a Json with the given content
     *
     * @param value Content of the Json
     */
    public Json(String value)
    {
        data = new JSONObject(value);
    }

    /**
     * Create a Json with the given content
     *
     * @param value Content of the Json
     */
    public Json(Map value)
    {
        data = new JSONObject(value);
    }

    /**
     * Create a Json from a stream
     *
     * @param stream Stream containing the json content
     */
    public Json(InputStream stream) throws IOException
    {
        data = new JSONObject(IOUtils.toString(stream, StandardCharsets.UTF_8));
    }

    /**
     * Create a Json from the given file
     *
     * @param file File containing the Json
     */
    public Json(File file) throws IOException
    {
        data = new JSONObject(IOUtils.toString(new FileInputStream(file), StandardCharsets.UTF_8));
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof Json))
            return false;
        return toString().equals(obj.toString());
    }

    @SuppressWarnings("EmptyCatchBlock")
    private void writeToFile(String path, int indent, boolean async)
    {
		Runnable task = () ->
		{
			String content = toString(indent);
			try
			{
				File file = new File(path);
				if (file.exists())
                    file.delete();
                FileUtils.writeStringToFile(file, content, "UTF-8");
			} catch (Exception e) {}
		};

		if (async)
		{
			Thread thread = new Thread(task);
			thread.start();
		}
		else
			task.run();
    }

    /**
     * Write the {@link Json} to the given file
     *
     * @param path Path of the file to create/overwrite
     */
    public void toFile(String path)
    {
        writeToFile(path, 4, false);
    }

    /**
     * Write asynchronously the {@link Json} to the given file
     *
     * @param path Path of the file to create/overwrite
     */
    public void toAsyncFile(String path)
    {
        writeToFile(path, 4, true);
    }

    /**
     * Write the {@link Json} to the given file with the given indentation
     *
     * @param path Path of the file to create/overwrite
     * @param indent Indentation of the file
     */
    public void toFile(String path, int indent)
    {
        writeToFile(path, indent, false);
    }

    /**
     * Write asynchronously the {@link Json} to the given file with the given indentation
     *
     * @param path Path of the file to create/overwrite
     * @param indent Indentation of the file
     */
    public void toAsyncFile(String path, int indent)
    {
        writeToFile(path, indent, true);
    }

    /**
     * Return the current Json formatted as {@link Map}
     *
     * @return The formatted Json
     */
    public Map<String, Object> toMap()
    {
        return data.toMap();
    }

    /**
     * Merge the given {@link Json} into this one
     *
     * @param json {@link Json} to merge
     */
    public void merge(Json json)
    {
        List<String> keys = json.getKeys();
        for (String key : keys)
        {
            Object ret = json.get(key);
            Object current = get(key);
            if (current == null)
                add(key, ret);
            else if (current.getClass() == ret.getClass())
            {
                if (ret instanceof List)
                    ((List)current).addAll((List)ret);
                else if (ret instanceof Json)
                    ((Json)current).merge((Json)ret);
                add(key, current, true);
            }
        }
    }

    @Override
    public String toString()
    {
        return data.toString();
    }

    /**
     * Return the current Json formatted as {@link String} using given indentation
     *
     * @param indentFactor Json indentation factor
     * @return The formatted Json
     */
    public String toString(int indentFactor)
    {
        return data.toString(indentFactor);
    }

    /**
     * Specify which json table to use
     * <br>Example:
     * <br>Path.To.Table
     *
     * @param key Path to the json table
     */
    public void use(String key)
    {
        Object value = get(key);
        if (value instanceof Json)
            data = ((Json)value).data;
    }

    /**
     * Return the data associated to the given key
     * <br>Example:
     * <br>Path.To.Data
     *
     * @param key Path to the json data
     * @return The expected data
     */
    private <T> Object getData(String key)
    {
        String search = key.split("\\.")[0];
        Object ret;
        try
        {
            ret = data.get(search);
        } catch (JSONException e)
        {
            return null;
        }
        if (key.equals(search))
        {
            if (ret == JSONObject.NULL)
                return null;
            else if (ret instanceof JSONObject)
                return new Json((JSONObject)ret);
            else if (ret instanceof JSONArray)
            {
                JSONArray array = (JSONArray)ret;
                List<T> list = new ArrayList<>();
                for (int n = 0; n != array.length(); ++n)
                {
                    Object elem = array.get(n);
                    if (elem instanceof JSONObject)
                        list.add((T)new Json((JSONObject)elem));
                    else
                        list.add((T)elem);
                }
                return list;
            }
            return ret;
        }
        return new Json((JSONObject)ret).getData(key.substring(search.length() + 1));
    }

    /**
     * Return the data associated to the given key
     * <br>Example:
     * <br>Path.To.Data
     *
     * @param key Path to the json data
     * @return The expected data
     */
    public <T> T get(String key)
    {
        return get(key, null);
    }

	/**
	 * Return the data associated to the given key if the key exist otherwise return the given default value
	 * <br>Example:
	 * <br>Path.To.Data
	 *
	 * @param key Path to the json data
	 * @param defaultValue Default value if the key
	 * @return The expected data
	 */
	public <T> T get(String key, T defaultValue)
	{
		T ret = (T)this.getData(key);
		if (ret == null)
			return defaultValue;
		return ret;
	}


	/**
     * Handle the data associated to the given key
     * <br>Example:
     * <br>Path.To.Data
     *
     * @param key Path to the json data
     * @param handler Class that will handle the result if not null
     */
    public <T> void handle(String key, Handler<T> handler)
    {
        T result = (T)this.getData(key);
        if (result != null)
            handler.handle(result);
    }

	/**
	 * Return true if the given key exist
	 * <br>Example:
	 * <br>Path.To.Data
	 *
	 * @param key Path to the json data
	 */
	public boolean find(String key)
	{
		return (this.getData(key) != null);
	}

    /**
     * Run an action if the given key exist
     * <br>Example:
     * <br>Path.To.Data
     *
     * @param key Path to the json data
     * @param handler Action that will be run if the key exist
     */
    public void ifExist(String key, ActionHandler handler)
    {
        if (find(key))
            handler.handle();
    }

	/**
	 * Run an action if the given key does not exist
	 * <br>Example:
	 * <br>Path.To.Data
	 *
	 * @param key Path to the json data
	 * @param handler Action that will be run if the key does not exist
	 */
	public void ifNotExist(String key, ActionHandler handler)
	{
		if (!find(key))
			handler.handle();
	}

    /**
     * Return the array of data associated to the given key
     * <br>Example:
     * <br>Path.To.Data
     *
     * @param key Path to the json data
     * @return The expected array of data
     */
    public <T> List<T> getArray(String key)
    {
        return (List<T>)this.<T>getData(key);
    }

    /**
     * Handle the array of data associated to the given key
     * <br>Example:
     * <br>Path.To.Data
     *
     * @param key Path to the json data
     * @param handler Class that will handle the array of result if not null
     */
    public <T> void handleArray(String key, Handler<List<T>> handler)
    {
        List<T> result = (List<T>)this.<T>getData(key);
        if (result != null)
            handler.handle(result);
    }

    /**
     * Handle each element of the array of data associated to the given key
     * <br>Example:
     * <br>Path.To.Data
     *
     * @param key Path to the json data
     * @param handler Class that will handle each result from the array of data if not null
     */
    public <T> void handleEach(String key, Handler<T> handler)
    {
        List<T> result = (List<T>)this.<T>getData(key);
        if (result != null)
        {
            for (T elem : result)
            {
                if (elem != null)
                    handler.handle(elem);
            }
        }
    }

    /**
     * Handle each keys in the json
     *
     * @param handler Class that will handle each keys and their associated value
     */
    public void handleKeys(KeyHandler handler)
    {
        Iterator<String> keys = data.keys();
        while (keys.hasNext())
        {
            String key = keys.next();
            handler.handle(key, data.get(key));
        }
    }

    private JSONArray convertList(List list)
    {
        JSONArray array = new JSONArray();
        for (Object elem : list)
        {
            if (elem instanceof Json)
                elem  = ((Json)elem).data;
            else if (elem instanceof List)
                elem = convertList((List)elem);
            array.put(elem);
        }
        return array;
    }

	/**
	 * Add the given value at the given key in the json
	 *
	 * @param key Path of the data to add
	 * @param valueToAdd Data to add
	 * @return true if the data has been successfully added
	 */
	@SuppressWarnings("EmptyCatchBlock")
	public boolean add(String key, Object valueToAdd)
	{
		return add(key, valueToAdd, true);
	}

    /**
     * Add the given value at the given key in the json
     *
     * @param key Path of the data to add
     * @param valueToAdd Data to add
     * @return true if the data has been successfully added
     */
    @SuppressWarnings("EmptyCatchBlock")
    public boolean add(String key, Object valueToAdd, boolean crush)
    {
        Object value;
        String[] keys = key.split("\\.");
        JSONObject map = data;
        for (int n = 0; n != (keys.length - 1); ++n)
        {
            try
            {
                value = map.get(keys[n]);
                if (value instanceof JSONObject)
                    map = (JSONObject)value;
                else
                    return false;
            } catch (Exception e)
            {
                JSONObject newSection = new JSONObject();
                map.put(keys[n], newSection);
                map = newSection;
            }
        }
        if (valueToAdd instanceof Json)
            valueToAdd = ((Json)valueToAdd).data;
		else if (valueToAdd instanceof List)
			valueToAdd = convertList((List)valueToAdd);
		if (!crush)
		{
			try
			{
				value = map.get(keys[keys.length - 1]);
				if (value instanceof JSONArray)
				{
					JSONArray array = (JSONArray) value;
					if (valueToAdd instanceof JSONArray)
					{
						JSONArray secondArray = (JSONArray)valueToAdd;
						for (int n = 0; n != secondArray.length(); ++n)
							array.put(secondArray.get(n));
					}
					else
						array.put(valueToAdd);
					valueToAdd = array;
				}
				else if (value.getClass() == valueToAdd.getClass())
				{
					JSONArray array = new JSONArray();
					array.put(value);
					array.put(valueToAdd);
					valueToAdd = array;
				}
			}
			catch (Exception e) {}
		}
        map.put(keys[keys.length - 1], valueToAdd);
        return true;
    }

    /**
     * Return an {@link Iterator} containing all the keys from the loaded json
     *
     * @return All the keys from the loaded json
     */
    public List<String> getKeys()
    {
        List<String> ret = new ArrayList<>();
        Iterator<String> keys = data.keys();
        while (keys.hasNext())
            ret.add(keys.next());
        return ret;
    }

    public interface ActionHandler
    {
        void handle();
    }

    public interface Handler<T>
    {
        void handle(T result);
    }

    public interface KeyHandler
    {
        void handle(String key, Object result);
    }
}