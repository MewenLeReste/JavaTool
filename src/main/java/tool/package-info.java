/**
 * A set of tool for your daily Java
 * <br>Tool allow you to manage:
 * <br>Manage your applications with {@link tool.application}
 * <br>Manage your documents with {@link tool.ms}
 * <br>Manage your Input/Output with {@link tool.shell}
 * <br>Encrypt/Decrypt your strings with {@link tool.encryption}
 */
package tool;