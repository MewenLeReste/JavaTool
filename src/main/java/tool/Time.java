package tool;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import tool.ms.spreadsheet.SpreadSheet;

/**
 * A equivalent of {@link java.util.Date} wich allow you to work with a date.
 * <br>This class is the only known time component for {@link SpreadSheet}
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public final class Time
{
	private int year;
	private int month;
	private int day;
	private int hour;
	private int minute;
	private int second;
	private int millisecond;
	private final String[] monthsLong = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
	private final String[] monthsShort = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"};
	private final String[] days = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
	public static final int YEAR = 0;
	public static final int MONTH = 1;
	public static final int DAY = 2;
	public static final int HOUR = 3;
	public static final int MINUTE = 4;
	public static final int SECOND = 5;
	public static final int MILLISECOND = 6;
	//==========Format==========\\
	public static final String CONSOLE = "[hh:min:ss]";
	public static final String ABSOLUTE = "ddmmyy_hh_min_ss_mil";
	public static final String TIME = "dd/mm/yy hh:min:ss.mil";
	public static final String PATH = "_mm_yy";
	public static final String SQL = "yy-mm-dd";
	public static final String DATE = "yy/mm/dd";

	//====================Constructor===================\\
	/**
	 * Convert a string wich is in the format yyyy-mm-dd
	 * 
	 * @param date Date in the format yyyy-mm-dd
	 */
	public Time(String date)
	{
		setDate(date);
	}

	/**
	 * Copy a {@link Time}
	 * 
	 * @param date Time to copy
	 */
	public Time(Time date)
	{
		setDate(date);
	}

	/**
	 * Convert a {@link Date}
	 * 
	 * @param date {@link Date} to convert
	 */
	public Time(Date date)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		if (calendar.get(Calendar.AM_PM) == Calendar.AM)
			setDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DATE), calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND), calendar.get(Calendar.MILLISECOND));
		else
			setDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DATE), calendar.get(Calendar.HOUR) + 12, calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND), calendar.get(Calendar.MILLISECOND));
	}

	/**
	 * Initialize {@link Time}
	 * 
	 * @param year Year
	 * @param month Month
	 * @param day Day
	 */
	public Time(int year, int month, int day)
	{
		setDate(year, month, day, 0, 0, 0, 0);
	}

	/**
	 * Initialize {@link Time}
	 * 
	 * @param year Year
	 * @param month Month
	 * @param day Day
	 * @param hour Hour
	 * @param minute Minutes
	 * @param second Second
	 * @param millisecond Millisecond
	 */
	public Time(int year, int month, int day, int hour, int minute, int second, int millisecond)
	{
		setDate(year, month, day, hour, minute, second, millisecond);
	}

	/**
	 * Initialize {@link Time} with the current date
	 */
	public Time()
	{
		Calendar calendar = Calendar.getInstance();
		if (calendar.get(Calendar.AM_PM) == Calendar.AM)
			setDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DATE), calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND), calendar.get(Calendar.MILLISECOND));
		else
			setDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DATE), calendar.get(Calendar.HOUR) + 12, calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND), calendar.get(Calendar.MILLISECOND));
	}

	//====================toString===================\\
	/**
	 * Return a string in the format "mm/yyyy"
	 * 
	 * @return A string in the format "mm/yyyy"
	 */
	public String toString()
	{
		String month;
		String year;
		if (this.month < 10)
			month = "0" + this.month;
		else
			month = "" + this.month;
		year = "" + this.year;
		return (month + "/" + year);
	}

	private String getDayName()
    {
        int dayNumber;
        if (month >= 3)
            dayNumber = (((23 * month / 9) + 4 + day + year + (year / 4) - (year / 100) + (year / 400) - 2) % 7) - 1;
        else
            dayNumber = (((23 * month / 9) + 4 + day + year + ((year - 1) / 4) - ((year - 1) / 100) + ((year - 1) / 400)) % 7) - 1;
        dayNumber = (dayNumber == -1) ? 6 : dayNumber;
        return days[dayNumber];
    }

	/**
	 * Return this {@link Time} at the given format with:
	 * <br>- DD representing the name of the day (Example: Monday)
	 * <br>- MMMM representing the full name of the month (Example: January)
	 * <br>- MM representing the short name of the month (Example: Jan)
	 * <br>- dd representing the day
	 * <br>- mm representing the month
	 * <br>- yy representing the year
	 * <br>- hh representing the hour
	 * <br>- min representing the minute
	 * <br>- ss representing the second
	 * <br>- mil representing the millisecond
	 * 
	 * @param format Wanted format for this {@link Time}
	 * @return This {@link Time} at the given format
	 */
	public String format(String format)
	{
		String day, month, year, hour, minute, second, millisecond;
		String dayName = getDayName();
		String monthShort = monthsShort[this.month - 1];
		String monthLong = monthsLong[this.month - 1];
		String ret = format;
		if (this.day < 10)
			day = "0" + this.day;
		else
			day = "" + this.day;
		if (this.month < 10)
			month = "0" + this.month;
		else
			month = "" + this.month;
		if (this.hour < 10)
			hour = "0" + this.hour;
		else
			hour = "" + this.hour;
		if (this.minute < 10)
			minute = "0" + this.minute;
		else
			minute = "" + this.minute;
		if (this.second < 10)
			second = "0" + this.second;
		else
			second = "" + this.second;
		if (this.millisecond < 10)
			millisecond = "00" + this.millisecond;
		else if (this.millisecond < 100)
			millisecond = "0" + this.millisecond;
		else
			millisecond = "" + this.millisecond;
		year = "" + this.year;
		ret = ret.replaceAll("DD", dayName);
		ret = ret.replaceAll("MMMM", monthLong);
		ret = ret.replaceAll("MM", monthShort);
		ret = ret.replaceAll("dd", day);
		ret = ret.replaceAll("mm", month);
		ret = ret.replaceAll("yy", year);
		ret = ret.replaceAll("hh", hour);
		ret = ret.replaceAll("min", minute);
		ret = ret.replaceAll("ss", second);
		ret = ret.replaceAll("mil", millisecond);
		return ret;
	}

	//====================Comparator===================\\
	/**
	 * Compare an {@link Object} to this {@link Time}
	 * 
	 * @return true if the given {@link Object} is a {@link Time} with the same value
	 */
	public boolean equals(Object obj)
	{
		if (!obj.getClass().equals(Time.class))
			return false;
		Time compare = (Time) obj;
		return (compare.get(YEAR) == year &&
			compare.get(MONTH) == month &&
			compare.get(DAY) == day &&
			compare.get(HOUR) == hour &&
			compare.get(MINUTE) == minute &&
			compare.get(SECOND) == second &&
			compare.get(MILLISECOND) == millisecond);
	}

	/**
	 * Compare an {@link Object} to this {@link Time}
	 * 
	 * @param obj {@link Object} to compare
	 * @return true if the given {@link Object} is a {@link Time} before this {@link Time}
	 */
	public boolean isBefore(Object obj)
	{
		if (!obj.getClass().equals(Time.class))
			return false;
		Time time = (Time) obj;
		if (time.get(YEAR) > year)
			return true;
		else if (time.get(YEAR) < year)
			return false;
		if (time.get(MONTH) > month)
			return true;
		else if (time.get(MONTH) < month)
			return false;
		if (time.get(DAY) > day)
			return true;
		else if (time.get(DAY) < day)
			return false;
		if (time.get(HOUR) > hour)
			return true;
		else if (time.get(HOUR) < hour)
			return false;
		if (time.get(MINUTE) > minute)
			return true;
		else if (time.get(MINUTE) < minute)
			return false;
		if (time.get(SECOND) > second)
			return true;
		else if (time.get(SECOND) < second)
			return false;
		if (time.get(MILLISECOND) > millisecond)
			return true;
		else if (time.get(MILLISECOND) < millisecond)
			return false;
		return false;
	}

	/**
	 * Compare an {@link Object} to this {@link Time}
	 * 
	 * @param obj {@link Object} to compare
	 * @return true if the given {@link Object} is a {@link Time} after this {@link Time}
	 */
	public boolean isAfter(Object obj)
	{
		if (!obj.getClass().equals(Time.class))
			return false;
		Time time = (Time) obj;
		if (time.get(YEAR) < year)
			return true;
		else if (time.get(YEAR) > year)
			return false;
		if (time.get(MONTH) < month)
			return true;
		else if (time.get(MONTH) > month)
			return false;
		if (time.get(DAY) < day)
			return true;
		else if (time.get(DAY) > day)
			return false;
		if (time.get(HOUR) < hour)
			return true;
		else if (time.get(HOUR) > hour)
			return false;
		if (time.get(MINUTE) < minute)
			return true;
		else if (time.get(MINUTE) > minute)
			return false;
		if (time.get(SECOND) < second)
			return true;
		else if (time.get(SECOND) > second)
			return false;
		if (time.get(MILLISECOND) < millisecond)
			return true;
		else if (time.get(MILLISECOND) > millisecond)
			return false;
		return false;
	}

	//====================Getter===================\\
	/**
	 * Return one of the value of this {@link Time}
	 * 
	 * @param part Searched value of this {@link Time}
	 * @return One of the value of this {@link Time}
	 */
	public int get(int part)
	{
		if (part == YEAR)
			return year;
		else if (part == MONTH)
			return month;
		else if (part == DAY)
			return day;
		else if (part == HOUR)
			return hour;
		else if (part == MINUTE)
			return minute;
		else if (part == SECOND)
			return second;
		else if (part == MILLISECOND)
			return millisecond;
		return -1;
	}

	/**
	 * Return a {@link Date} corresponding to this {@link Time}
	 * 
	 * @return A {@link Date} corresponding to this {@link Time}
	 */
	public Date getDate() throws ParseException
	{
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		return format.parse(format("dd/mm/yy"));
	}

	/**
	 * Return a new {@link Time} corresponding to this {@link Time}
	 * 
	 * @return A new {@link Time} corresponding to this {@link Time}
	 */
	public Time getTime()
	{
		return new Time(year, month, day, hour, minute, second, millisecond);
	}
	
	//====================Increase===================\\
	/**
	 * Increase one of the value of this {@link Time}
	 * 
	 * @param part Searched value of this {@link Time}
	 */
	public void add(int part)
	{
		if (part == YEAR)
			year++;
		else if (part == MONTH)
			addMonth();
		else if (part == DAY)
			addDay();
		else if (part == HOUR)
			addHour();
		else if (part == MINUTE)
			addMinute();
		else if (part == SECOND)
			addSecond();
		else if (part == MILLISECOND)
			addMillisecond();
	}

	private void addMonth()
	{
		month++;
		if (month >= 13)
		{
			month = 1;
			year++;
		}
	}

	private void addDay()
	{
		day++;
		if ((day >= 30 && month == 2 && (year % 4) == 0) ||
			(day >= 29 && month == 2) ||
			(day >= 31 && (month == 4 || month == 6 || month == 9 || month == 11)) ||
			(day >= 32 && (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)))
		{
			day = 1;
			addMonth();
		}
	}

	private void addHour()
	{
		hour++;
		if (hour >= 24)
		{
			hour = 0;
			addDay();
		}
	}

	private void addMinute()
	{
		minute++;
		if (minute >= 60)
		{
			minute = 0;
			addHour();
		}
	}

	private void addSecond()
	{
		second++;
		if (second >= 60)
		{
			second = 0;
			addMinute();
		}
	}

	private void addMillisecond()
	{
		millisecond++;
		if (millisecond >= 1000)
		{
			millisecond = 0;
			addSecond();
		}
	}

	//====================Decrease===================\\
	/**
	 * Decrease one of the value of this {@link Time}
	 * 
	 * @param part Searched value of this {@link Time}
	 */
	public void decrease(int part)
	{
		if (part == YEAR)
			year--;
		else if (part == MONTH)
			decreaseMonth();
		else if (part == DAY)
			decreaseDay();
		else if (part == HOUR)
			decreaseHour();
		else if (part == MINUTE)
			decreaseMinute();
		else if (part == SECOND)
			decreaseSecond();
		else if (part == MILLISECOND)
			decreaseMillisecond();
	}

	private void decreaseMonth()
	{
		month--;
		if (month <= 0)
		{
			month = 12;
			year--;
		}
	}

	private void decreaseDay()
	{
		day--;
		if (day <= 0)
		{
			decreaseMonth();
			if (month == 2 && (year % 4) == 0)
				day = 29;
			else if (month == 2)
				day = 28;
			else if (month == 4 || month == 6 || month == 9 || month == 11)
				day = 30;
			else
				day = 31;
		}
	}

	private void decreaseHour()
	{
		hour--;
		if (hour <= -1)
		{
			hour = 23;
			decreaseDay();
		}
	}

	private void decreaseMinute()
	{
		minute--;
		if (minute <= -1)
		{
			minute = 59;
			decreaseHour();
		}
	}

	private void decreaseSecond()
	{
		second--;
		if (second <= -1)
		{
			second = 59;
			decreaseMinute();
		}
	}

	private void decreaseMillisecond()
	{
		millisecond--;
		if (millisecond <= -1)
		{
			millisecond = 999;
			decreaseSecond();
		}
	}

	//====================Setter===================\\
	/**
	 * Set one of the value of this {@link Time} to the given one
	 * 
	 * @param part Searched value of this {@link Time}
	 * @param value Value to put
	 */
	public void set(int part, int value)
	{
		if (part == YEAR)
			setYear(value);
		else if (part == MONTH)
			setMonth(value);
		else if (part == DAY)
			setDay(value);
		else if (part == HOUR)
			setHour(value);
		else if (part == MINUTE)
			setMinute(value);
		else if (part == SECOND)
			setSecond(value);
		else if (part == MILLISECOND)
			setMillisecond(value);
	}

	public void setYear(int year)
	{
		this.year = year;
	}

	public void setMonth(int month)
	{
		this.month = month % 13;
	}

	public void setDay(int day)
	{
		if (month == 2 && (year % 4) == 0)
			this.day = day % 30;
		else if (month == 2)
			this.day = day % 29;
		else if (month == 4 || month == 6 || month == 9 || month == 11)
			this.day = day % 31;
		else
			this.day = day % 32;
	}

	public void setHour(int hour)
	{
		this.hour = hour % 24;
	}

	public void setMinute(int minute)
	{
		this.minute = minute % 60;
	}

	public void setSecond(int second)
	{
		this.second = second % 60;
	}

	public void setMillisecond(int millisecond)
	{
		this.millisecond = millisecond % 1000;
	}
	/**
	 * Initialize this {@link Time} with a string in th format "yyyyXmmXdd"
	 * Where X is a character
	 * 
	 * @param date String in the format "yyyyXmmXdd"
	 */
	public void setDate(String date)
	{
		if (date == null)
		{
			setDate(1970, 1, 1, 0, 0, 0, 0);
			return;
		}
		String tmp;
		tmp = date.substring(0, 4);
		int year = Integer.parseInt(tmp);
		tmp = date.substring(5, 7);
		int month = Integer.parseInt(tmp);
		tmp = date.substring(8, 10);
		int day = Integer.parseInt(tmp);
		setDate(year, month, day, 0, 0, 0, 0);
	}

	/**
	 * Initialize this {@link Time} with a other {@link Time}
	 * 
	 * @param date Time to copy
	 */
	public void setDate(Time date)
	{
		setYear(date.get(YEAR));
		setMonth(date.get(MONTH));
		setDay(date.get(DAY));
		setHour(date.get(HOUR));
		setMinute(date.get(MINUTE));
		setSecond(date.get(SECOND));
		setMillisecond(date.get(MILLISECOND));
	}

	public void setDate(int year, int month, int day, int hour, int minute, int second, int millisecond)
	{
		setYear(year);
		setMonth(month);
		setDay(day);
		setHour(hour);
		setMinute(minute);
		setSecond(second);
		setMillisecond(millisecond);
	}
	
	/**
	 * Return a {@link Time} equivalent of the difference between the given {@link Time}
	 * @param time1 {@link Time} to compare
	 * @param time2 {@link Time} to compare
	 * @return A {@link Time} equivalent of the difference between the given {@link Time}
	 */
	public static Time diff(Time time1, Time time2)
	{
		Time diff = new Time(0, 0, 0, 0, 0, 0, 0);
		Time toDiff = new Time(0, 0, 0, 0, 0, 0, 0);
		if (time1.isAfter(time2))
		{
			diff.setDate(time1);
			toDiff.setDate(time2);
		}
		else if (time1.isBefore(time2))
		{
			diff.setDate(time2);
			toDiff.setDate(time1);
		}
		for (int i = 6; i >= 0; i--)
		{
			if (diff.get(i) != toDiff.get(i))
			{
				for (int n = 0; n < toDiff.get(i); n++)
					diff.decrease(i);
			}
			else
				diff.set(i, 0);
		}
		return diff;
	}
}