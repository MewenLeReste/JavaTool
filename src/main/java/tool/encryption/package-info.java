/**
 * A class that allow you to encrypt/decrypt your string with:
 * <br>Cipher encryption
 * <br>
 * <br>And to load/save your crypted string in a file
 */
package tool.encryption;