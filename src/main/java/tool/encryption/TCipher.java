package tool.encryption;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.Exception;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.nio.charset.StandardCharsets;

public class TCipher
{
	public static final String AES = "AES";
	public static final String AESWRAP = "AESWrap";
	public static final String ARCFOUR = "ARCFOUR";
	public static final String BLOWFISH = "Blowfish";
	public static final String CCM = "CCM";
	public static final String DES = "DES";
	public static final String DESEDE = "DESede";
	public static final String DESEDEWRAP = "DESedeWrap";
	public static final String ECIES = "ECIES";
	public static final String GCM = "GCM";
	public static final String RC2 = "RC2";
	public static final String RC4 = "RC4";
	public static final String RC5 = "RC5";
	public static final String RSA = "RSA";
	private SecretKeySpec secretKey;
	private Cipher cipher;

	public TCipher(String key) throws Exception
	{
		secretKey = new SecretKeySpec(key.getBytes(), "AES");
		cipher = Cipher.getInstance("AES");
	}

	public TCipher(String key, String algorithm) throws Exception
	{
		secretKey = new SecretKeySpec(key.getBytes(), algorithm);
		cipher = Cipher.getInstance(algorithm);
	}

	/**
	 * Encrypt the given {@link String} into a file
	 *
	 * @param fileName Name of the file to write to
	 * @param content {@link String} to encrypt
	 */
	public void saveEncryptedFile(String fileName, String content) throws Exception
	{
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		byte[] outputBytes = cipher.doFinal(content.getBytes(StandardCharsets.ISO_8859_1));
		FileOutputStream outputStream = new FileOutputStream(fileName);
		outputStream.write(outputBytes);
		outputStream.close();
	}

	/**
	 * Decrypt the given file
	 *
	 * @param fileName Path to the file to decrypt
	 * @return The decrypted file
	 */
	public String loadEncryptedFile(String fileName) throws Exception
	{
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		File inputFile = new File(fileName);
		FileInputStream inputStream = new FileInputStream(inputFile);
		byte[] inputBytes = new byte[(int)inputFile.length()];
		if (inputStream.read(inputBytes) == 0)
			return "";
		return new String(cipher.doFinal(inputBytes), StandardCharsets.ISO_8859_1);
	}

	/**
	 * Encrypt the given {@link String}
	 *
	 * @param content {@link String} to encrypt
	 * @return The encrypted {@link String}
	 */
	public String encrypt(String content) throws Exception
	{
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		return new String(cipher.doFinal(content.getBytes(StandardCharsets.ISO_8859_1)), StandardCharsets.ISO_8859_1);
	}

	/**
	 * Decrypt the given {@link String}
	 *
	 * @param content {@link String} to decrypt
	 * @return The decrypted {@link String}
	 */
	public String decrypt(String content) throws Exception
	{
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		return new String(cipher.doFinal(content.getBytes(StandardCharsets.ISO_8859_1)), StandardCharsets.ISO_8859_1);
	}
}
