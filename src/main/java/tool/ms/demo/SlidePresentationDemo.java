package tool.ms.demo;

import tool.ms.slideshow.SlidePresentation;

public class SlidePresentationDemo
{
	private static final String PPTNAME = "JavaToolTestPPT.ppt";
	private static final String PPTXNAME = "JavaToolTestPPTX.pptx";

	@SuppressWarnings("WeakerAccess")
	public SlidePresentationDemo()
	{
		String path = System.getProperty("user.home") + "/Desktop/";
		SlidePresentation pptx = new SlidePresentation(path + PPTNAME, true);
		SlidePresentation ppt = new SlidePresentation(path + PPTXNAME, true);
		generate(pptx);
		generate(ppt);
		System.out.println(PPTNAME + " and " + PPTXNAME + " has been generated to: " + path);
	}
	
	private void generate(SlidePresentation slideshow)
	{
		slideshow.addSlide();
		slideshow.save();
		slideshow.close();
	}

	public static void main(String[] args)
	{
		new SlidePresentationDemo();
	}
}
