package tool.ms.demo;

import java.awt.Color;

import tool.Time;
import tool.ms.spreadsheet.CellAlignment;
import tool.ms.spreadsheet.CellBorderStyle;
import tool.ms.spreadsheet.CellDataFormat;
import tool.ms.spreadsheet.CellFillPattern;
import tool.ms.spreadsheet.CellFormat;
import tool.ms.spreadsheet.SpreadSheetConditionalFormat;
import tool.ms.spreadsheet.ConditionalIconSet3;
import tool.ms.spreadsheet.ConditionalIconFormat;
import tool.ms.spreadsheet.Percent;
import tool.ms.spreadsheet.SpreadSheet;
import tool.ms.spreadsheet.SpreadSheetColor;
import tool.ms.spreadsheet.SpreadSheetCursor;
import tool.ms.spreadsheet.SpreadSheetFormat;
import tool.ms.spreadsheet.SpreadSheetFormula;
import tool.ms.spreadsheet.SpreadSheetImage;
import tool.ms.spreadsheet.SpreadSheetPosition;
import tool.ms.spreadsheet.SpreadSheetRange;

/**
 * A test for {@link SpreadSheet} which create on the Desktop a xls and an xlsx file created with the library
 */
@SuppressWarnings({"unused", "UnusedReturnValue"})
public class SpreadSheetDemo
{
	private static final String XLS_NAME = "JavaToolTestXLS.xls";
	private static final String XLSX_NAME = "JavaToolTestXLSX.xlsx";

	public SpreadSheetDemo(String imgPath)
	{
		init(imgPath);
	}

    @SuppressWarnings("WeakerAccess")
	public SpreadSheetDemo()
	{
		init(null);
	}
	
	private void init(String imgPath)
	{
		String path = System.getProperty("user.home") + "/Desktop/";
		SpreadSheet xlsx = new SpreadSheet(path + XLSX_NAME, true);
		SpreadSheet xls = new SpreadSheet(path + XLS_NAME, true);
		generate(xlsx, imgPath);
		generate(xls, imgPath);
		System.out.println(XLS_NAME + " and " + XLSX_NAME + " has been generated to: " + path);
	}

	private SpreadSheetCursor testCellFormat(SpreadSheetCursor cursor, SpreadSheet spreadsheet)
	{
		Percent percent = new Percent("42%");
		SpreadSheetFormat format = new SpreadSheetFormat(); 
		SpreadSheetPosition pos = new SpreadSheetPosition(cursor.getPage(), "B2");
		CellFormat cell1 = new CellFormat(pos, false, false);
		cell1.setBold(true);
		cell1.setItalic(true);
		cell1.setUnderline(true, true);
		cell1.setStrikeout(true);
		cell1.setBackground(CellFillPattern.SOLID, new SpreadSheetColor(255, 121, 0));
		cell1.setFont("Arial");
		cell1.setSize(25);
		cell1.setTextColor(SpreadSheetColor.RED);
		cell1.setAlignment(CellAlignment.TOPRIGHT);
		cell1.setTopBorder(CellBorderStyle.DASH_DOT, SpreadSheetColor.GREEN);
		cell1.setBottomBorder(CellBorderStyle.THICK, SpreadSheetColor.YELLOW);
		cell1.setLeftBorder(CellBorderStyle.DOTTED, Color.CYAN);
		cell1.setRightBorder(CellBorderStyle.HAIR, Color.MAGENTA);
		format.add(cell1);
		spreadsheet.newPage();
		spreadsheet.setColumnWidth(cursor.getPage(), 1, 400);
		spreadsheet.setRowHeight(cursor.getPage(), 1, 400);
		spreadsheet.addContent(format, pos, percent);
		spreadsheet.addContent(cursor, 0.42);
		cursor.nextRow();
		spreadsheet.addContent(cursor, "Test");
		cursor.nextRow();
		spreadsheet.addContent(cursor, new Time());
		return cursor;
	}
	
	private SpreadSheetCursor testGroupCellsAndRange(SpreadSheetCursor cursor, SpreadSheet spreadsheet)
	{
		spreadsheet.newPage();
		SpreadSheetRange range = new SpreadSheetRange(cursor.getPage(), "B4:D6");
		SpreadSheetFormat format = new SpreadSheetFormat();
		CellFormat cellFormat = new CellFormat(range, false);
		cellFormat.setBorder(CellBorderStyle.MEDIUM, SpreadSheetColor.RED);
		format.add(cellFormat);
		spreadsheet.addContent(format, range, "Test");
		range = new SpreadSheetRange(cursor.getPage(), "B8:D10");
		cellFormat = new CellFormat(range, true);
		cellFormat.setBorder(CellBorderStyle.MEDIUM, SpreadSheetColor.RED);
		format.add(cellFormat);
		spreadsheet.addContent(format, range, "Test");
		spreadsheet.groupRow(cursor.getPage(), 3, 5);
		spreadsheet.groupColumn(cursor.getPage(), 1, 3);
		return cursor;
	}
	
	private SpreadSheetCursor testFreezeCells(SpreadSheetCursor cursor, SpreadSheet spreadsheet)
	{
		spreadsheet.newPage();
		cursor.setCell("A5");
		spreadsheet.addContent(cursor, "Test");
		cursor.previousRow();
		spreadsheet.addContent(cursor, "Test");
		cursor.previousRow();
		spreadsheet.addContent(cursor, "Test");
		cursor.previousRow();
		spreadsheet.addContent(cursor, "Test");
		cursor.previousRow();
		spreadsheet.addContent(cursor, "Test");
		cursor.nextColumn();
		spreadsheet.addContent(cursor, "Test");
		cursor.nextColumn();
		spreadsheet.addContent(cursor, "Test");
		cursor.nextColumn();
		spreadsheet.addContent(cursor, "Test");
		cursor.nextColumn();
		spreadsheet.addContent(cursor, "Test");
		spreadsheet.freezeColumn(3, 1);
		spreadsheet.freezeRow(3, 1);
		return cursor;
	}
	
	private SpreadSheetCursor testMergeCells(SpreadSheetCursor cursor, SpreadSheet spreadsheet)
	{
		spreadsheet.newPage();
		SpreadSheetFormat mergeFormat = new SpreadSheetFormat();
		CellFormat cellA1 = new CellFormat(new SpreadSheetPosition(cursor.getPage(), "A1"), false, false);
		CellFormat cellA2 = new CellFormat(new SpreadSheetPosition(cursor.getPage(), "A2"), false, false);
		CellFormat cellB1 = new CellFormat(new SpreadSheetPosition(cursor.getPage(), "B1"), false, false);
		CellFormat cellB2 = new CellFormat(new SpreadSheetPosition(cursor.getPage(), "B2"), false, false);
		cellA1.setBackground(CellFillPattern.SOLID, SpreadSheetColor.AQUA);
		cellA2.setBackground(CellFillPattern.SOLID, SpreadSheetColor.YELLOW);
		cellB1.setBackground(CellFillPattern.SOLID, SpreadSheetColor.GREEN);
		cellB2.setBackground(CellFillPattern.SOLID, SpreadSheetColor.RED);
		mergeFormat.add(cellA1);
		mergeFormat.add(cellA2);
		mergeFormat.add(cellB1);
		mergeFormat.add(cellB2);
		spreadsheet.mergeCells(4, 0, 1, 0, 1);
		cursor.setCell("A1");
		spreadsheet.addContent(mergeFormat, cursor, "TestA1");
		cursor.setCell("A2");
		spreadsheet.addContent(mergeFormat, cursor, "TestA2");
		cursor.setCell("B1");
		spreadsheet.addContent(mergeFormat, cursor, "TestB1");
		cursor.setCell("B2");
		spreadsheet.addContent(mergeFormat, cursor, "TestB2");
		return cursor;
	}
	
	private SpreadSheetCursor testFormula(SpreadSheetCursor cursor, SpreadSheet spreadsheet)
	{
		spreadsheet.newPage();
		cursor.setCell("A1");
		spreadsheet.addContent(cursor, 1);
		cursor.nextRow();
		spreadsheet.addContent(cursor, 8);
		cursor.nextRow();
		spreadsheet.addContent(cursor, new SpreadSheetFormula("A1+A2", CellDataFormat.NUMERICAL));
		return cursor;
	}
	
	private SpreadSheetCursor testImage(SpreadSheetCursor cursor, SpreadSheet spreadsheet, String imgPath)
	{
		if (imgPath != null)
		{
			spreadsheet.newPage();
			cursor.setCell("A1");
			SpreadSheetImage img = new SpreadSheetImage(imgPath);
			spreadsheet.addContent(cursor, img);
		}
		return cursor;
	}
	
	private SpreadSheetCursor testConditionalFormat(SpreadSheetCursor cursor, SpreadSheet spreadsheet)
	{
		spreadsheet.newPage();
		cursor.setCell("A1");
		Percent hundred = new Percent(100);
		Percent fifty = new Percent(50);
		Percent zero = new Percent(0);
		spreadsheet.addContent(cursor, hundred);
		cursor.nextRow();
		spreadsheet.addContent(cursor, hundred);
		cursor.nextRow();
		spreadsheet.addContent(cursor, hundred);
		cursor.nextRow();
		spreadsheet.addContent(cursor, fifty);
		cursor.nextRow();
		spreadsheet.addContent(cursor, fifty);
		cursor.nextRow();
		spreadsheet.addContent(cursor, fifty);
		cursor.nextRow();
		spreadsheet.addContent(cursor, zero);
		cursor.nextRow();
		spreadsheet.addContent(cursor, zero);
		cursor.nextRow();
		spreadsheet.addContent(cursor, zero);
		SpreadSheetRange range = new SpreadSheetRange(cursor.getPage(), "A1:A9");
		SpreadSheetConditionalFormat conditionalFormat = spreadsheet.getConditionalFormat(cursor.getPage(), range);
		if (conditionalFormat != null)
		{
			ConditionalIconFormat icon = conditionalFormat.getConditionalIconFormat(ConditionalIconSet3.COLORED_TRAFFIC_LIGHT);
			icon.addCondition(0, 0, true);
			icon.addCondition(1, 50, true);
			icon.addCondition(2, 100, true);
			conditionalFormat.setConditionalFormat();
		}
		return cursor;
	}
	
	private void generate(SpreadSheet spreadsheet, String imgPath)
	{
		SpreadSheetCursor cursor = new SpreadSheetCursor(SpreadSheetPosition.LAST, "B4");
		CellFormat defaultFormat = new CellFormat();
		defaultFormat.setFont("Calibri");
		defaultFormat.setSize(11);
		spreadsheet.setDefaultCellFormat(defaultFormat);
		testCellFormat(cursor, spreadsheet);
		testGroupCellsAndRange(cursor, spreadsheet);
		testFreezeCells(cursor, spreadsheet);
		testMergeCells(cursor, spreadsheet);
		testFormula(cursor, spreadsheet);
		testConditionalFormat(cursor, spreadsheet);
		testImage(cursor, spreadsheet, imgPath);
		spreadsheet.save();
		spreadsheet.close();
	}
	
	public static void main(String[] args)
	{
		new SpreadSheetDemo();
	}
}
