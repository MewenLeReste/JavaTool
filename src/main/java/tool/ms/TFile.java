package tool.ms;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;

import org.apache.commons.io.IOUtils;

/**
 * An equivalent of {@link File} for tool that recognize the JavaConsoleScript files
 * 
 * @see File
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class TFile extends File
{
	private static final long serialVersionUID = 1L;
	private String suffix;

	public TFile(File file)
	{
		super(file.getAbsolutePath());
		suffix = getName().substring(getName().lastIndexOf(".") + 1);
	}

	public TFile(String pathname)
	{
		super(pathname);
		suffix = getName().substring(getName().lastIndexOf(".") + 1);
	}

	public TFile(URI uri)
	{
		super(uri);
		suffix = getName().substring(getName().lastIndexOf(".") + 1);
	}

	public TFile(String parent, String child)
	{
		super(parent, child);
		suffix = getName().substring(getName().lastIndexOf(".") + 1);
	}

	public TFile(File parent, String child)
	{
		super(parent, child);
		suffix = getName().substring(getName().lastIndexOf(".") + 1);
	}
	
	@Override
	public TFile[] listFiles()
	{
		File[] files = super.listFiles();
		if (files == null)
		    return null;
		TFile[] tFiles = new TFile[files.length];
		for (int n = 0; n < files.length; n++)
            tFiles[n] = new TFile(files[n]);
		return tFiles;
	}
	
	/**
	 * Return the content of the file
	 * 
	 * @return The content of the file
	 */
	public String getContent()
	{
		try
		{
			return IOUtils.toString(new FileInputStream(this), "UTF-8");
		} catch (Exception e)
		{
		    e.printStackTrace();
			return null;
		}
	}

	/**
	 * Return the type of the file
	 * 
	 * @return The type of the file
	 */
	public final String getSuffix()
	{
		return suffix;
	}
}
