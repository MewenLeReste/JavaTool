package tool.ms.spreadsheet;

/**
 * A conditional format for a {@link SpreadSheetConditionalFormat}
 */
abstract class ConditionalFormat
{
	abstract void setFormat();
}
