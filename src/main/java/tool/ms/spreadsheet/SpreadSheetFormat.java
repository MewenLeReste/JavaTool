package tool.ms.spreadsheet;

import java.util.LinkedList;


/**
 * Format of all the cells of a {@link SpreadSheet} page
 */
public class SpreadSheetFormat
{
	private LinkedList<CellFormat> format;

	public SpreadSheetFormat()
	{
		format = new LinkedList<>();
	}

	/**
	 * Add a new {@link CellFormat}
	 * 
	 * @param cellFormat New {@link CellFormat}
	 */
	public void add(CellFormat cellFormat)
	{
		format.add(cellFormat);
	}

	CellFormat formatCell(SpreadSheetPosition pos)
	{
		for (CellFormat cellFormat : format)
		{
			if (cellFormat.isFormatable(pos))
				return cellFormat;
		}
		return null;
	}
}
