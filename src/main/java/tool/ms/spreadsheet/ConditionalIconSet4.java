package tool.ms.spreadsheet;

import org.apache.poi.ss.usermodel.IconMultiStateFormatting.IconSet;

/**
 * Set of 4 icons for the {@link ConditionalIconFormat}
 */
@SuppressWarnings("unused")
public class ConditionalIconSet4 extends ConditionalIconSet
{
	public static final ConditionalIconSet4 ARROWS = new ConditionalIconSet4(IconSet.GREY_4_ARROWS);
	public static final ConditionalIconSet4 COLORED_ARROWS = new ConditionalIconSet4(IconSet.GYR_4_ARROWS);
	public static final ConditionalIconSet4 COLORED_TRAFFIC_LIGHT = new ConditionalIconSet4(IconSet.GYRB_4_TRAFFIC_LIGHTS);
	public static final ConditionalIconSet4 RATING = new ConditionalIconSet4(IconSet.RATINGS_4);
	public static final ConditionalIconSet4 BICOLORED_TRAFFIC_LIGHT = new ConditionalIconSet4(IconSet.RB_4_TRAFFIC_LIGHTS);
	
	private ConditionalIconSet4(IconSet set)
	{
		super(set);
	}
}
