package tool.ms.spreadsheet;

import org.apache.poi.ss.usermodel.IconMultiStateFormatting.IconSet;

/**
 * Set of 5 icons for the {@link ConditionalIconFormat}
 */
@SuppressWarnings("unused")
public class ConditionalIconSet5 extends ConditionalIconSet
{
	public static final ConditionalIconSet5 ARROWS = new ConditionalIconSet5(IconSet.GREY_5_ARROWS);
	public static final ConditionalIconSet5 COLORED_ARROWS = new ConditionalIconSet5(IconSet.GYYYR_5_ARROWS);
	public static final ConditionalIconSet5 QUARTERS = new ConditionalIconSet5(IconSet.QUARTERS_5);
	public static final ConditionalIconSet5 RATING = new ConditionalIconSet5(IconSet.RATINGS_5);

	private ConditionalIconSet5(IconSet set)
	{
		super(set);
	}
}
