package tool.ms.spreadsheet;

/**
 * Class that symbolize an Excel percentage for {@link SpreadSheet}
 */
@SuppressWarnings("WeakerAccess")
public class Percent
{
	private double percent;

	/**
	 * Initialize the percent with the given value
	 * 
	 * @param percent Value of the percent
	 */
	public Percent(double percent)
	{
		this.percent = percent;
	}
	
	/**
	 * Initialize the percent with a string in the format "value%" or "value" / 100
	 * 
	 * @param percent String in the format "value%" or "value" / 100
	 */
	public Percent(String percent)
	{
		if (percent.contains("%"))
		{
			percent = percent.substring(0, percent.indexOf("%"));
			this.percent = Double.parseDouble(percent.replace(",", "."));
		}
		else
			this.percent = Double.parseDouble(percent.replace(",", ".")) * 100;
	}
	
	/**
	 * Return "value%"
	 * 
	 * @return "value%"
	 */
	public String toString()
	{
		return (percent + "%");
	}
	
	/**
	 * Return the percent
	 * 
	 * @return The percent
	 */
	public double getPercent()
	{
		return percent / 100;
	}
}
