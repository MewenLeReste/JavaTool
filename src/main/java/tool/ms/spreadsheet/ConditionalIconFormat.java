package tool.ms.spreadsheet;

import org.apache.poi.ss.usermodel.ConditionalFormattingThreshold.RangeType;
import org.apache.poi.xssf.usermodel.XSSFConditionalFormattingRule;
import org.apache.poi.xssf.usermodel.XSSFConditionalFormattingThreshold;
import org.apache.poi.xssf.usermodel.XSSFIconMultiStateFormatting;

/**
 * A {@link ConditionalFormat} for the use of icon for the conditional format
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class ConditionalIconFormat extends ConditionalFormat
{
	private XSSFIconMultiStateFormatting icon;
	private XSSFConditionalFormattingThreshold[] thresholds;
	public int max;
	
	ConditionalIconFormat(XSSFConditionalFormattingRule rule, ConditionalIconSet set)
	{
		int nbIcon;
		if (set.getClass().equals(ConditionalIconSet3.class))
			nbIcon = 3;
		else if (set.getClass().equals(ConditionalIconSet4.class))
			nbIcon = 4;
		else if (set.getClass().equals(ConditionalIconSet5.class))
			nbIcon = 5;
		else
			return ;
		this.icon = rule.createMultiStateFormatting(set.getSet());
		thresholds = new XSSFConditionalFormattingThreshold[nbIcon];
		for (int n = 0; n != nbIcon; n++)
		{
			thresholds[n] = icon.createThreshold();
			thresholds[n].setRangeType(RangeType.NUMBER);
			thresholds[n].setValue(0.0);
		}
		max = nbIcon;
	}

	/**
	 * Add a value for one of the condition
	 * 
	 * @param condition Position of the condition (first = this.min and RED and last = this.max and GREEN)
	 * @param value The value of the limit
	 * @param percent Specify if the value is a percent or not
	 */
	public void addCondition(int condition, double value, boolean percent)
	{
		if (percent)
			thresholds[condition].setRangeType(RangeType.PERCENT);
		else
			thresholds[condition].setRangeType(RangeType.NUMBER);
		thresholds[condition].setValue(value);
	}

	/**
	 * Add a formula for one of the condition
	 * 
	 * @param condition Position of the condition (first = this.min and RED and last = this.max and GREEN)
	 * @param formula The formula which will give the value
	 */
	public void addCondition(int condition, String formula)
	{
		thresholds[condition].setRangeType(RangeType.FORMULA);
		thresholds[condition].setFormula(formula);
	}

	@Override
	void setFormat()
	{
		icon.setThresholds(thresholds);
	}
}