package tool.ms.spreadsheet;

import org.apache.poi.ss.usermodel.IconMultiStateFormatting.IconSet;

@SuppressWarnings("WeakerAccess")
abstract class ConditionalIconSet
{
	private IconSet set;
	
	protected ConditionalIconSet(IconSet set)
	{
		this.set = set;
	}

	IconSet getSet()
	{
		return set;
	}
}
