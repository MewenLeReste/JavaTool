package tool.ms.spreadsheet;

/**
 * Class that symbolize an excel formula for {@link SpreadSheet}
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class SpreadSheetFormula
{
	private String formula;
	private CellDataFormat dataFormat;
	private Object value;

	SpreadSheetFormula(String formula, CellDataFormat dataFormat, Object value)
	{
		this.formula = formula;
		this.dataFormat = dataFormat;
		this.value = value;
	}
	
	public SpreadSheetFormula(String formula, CellDataFormat dataFormat)
	{
		this.formula = formula;
		this.dataFormat = dataFormat;
		value = null;
	}

	public String getFormula()
	{
		return formula;
	}

	public CellDataFormat getDataFormat()
	{
		return dataFormat;
	}
	
	public Object getValue()
	{
		return value;
	}
}
