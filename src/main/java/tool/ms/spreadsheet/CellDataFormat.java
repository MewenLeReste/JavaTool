package tool.ms.spreadsheet;

/**
 * Class containing all the data format known by the {@link CellFormat}
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class CellDataFormat
{
	static final CellDataFormat EMPTY = new CellDataFormat((short) -1);
	/**
	 * General
	 */
	public static final CellDataFormat GENERAL = new CellDataFormat((short) 0);
	/**
	 * 0
	 */
	public static final CellDataFormat NUMERICAL = new CellDataFormat((short) 1);
	/**
	 * 0.00
	 */
	public static final CellDataFormat DECIMAL = new CellDataFormat((short) 2);
	/**
	 * #,##0
	 */
	public static final CellDataFormat COMANUMERICAL = new CellDataFormat((short) 3);
	/**
	 * #,##0.00
	 */
	public static final CellDataFormat COMADECIMAL = new CellDataFormat((short) 4);
	/**
	 * $#,##0_);($#,##0)
	 */
	public static final CellDataFormat CUSTOM1 = new CellDataFormat((short) 5);
	/**
	 * $#,##0_);[Red]($#,##0)
	 */
	public static final CellDataFormat CUSTOM2 = new CellDataFormat((short) 6);
	/**
	 * $#,##0.00);($#,##0.00)
	 */
	public static final CellDataFormat CUSTOM3 = new CellDataFormat((short) 7);
	/**
	 * $#,##0.00_);[Red]($#,##0.00)
	 */
	public static final CellDataFormat CUSTOM4 = new CellDataFormat((short) 8);
	/**
	 * #,##0_);(#,##0)
	 */
	public static final CellDataFormat CUSTOM5 = new CellDataFormat((short) 0x25);
	/**
	 * #,##0_);[Red](#,##0)
	 */
	public static final CellDataFormat CUSTOM6 = new CellDataFormat((short) 0x26);
	/**
	 * #,##0.00_);(#,##0.00)
	 */
	public static final CellDataFormat CUSTOM7 = new CellDataFormat((short) 0x27);
	/**
	 * #,##0.00_);[Red](#,##0.00)
	 */
	public static final CellDataFormat CUSTOM8 = new CellDataFormat((short) 0x28);
	/**
	 * _(* #,##0_);_(* (#,##0);_(* \"-\"_);_(@_)
	 */
	public static final CellDataFormat CUSTOM9 = new CellDataFormat((short) 0x29);
	/**
	 * _($* #,##0_);_($* (#,##0);_($* \"-\"_);_(@_)
	 */
	public static final CellDataFormat CUSTOM10 = new CellDataFormat((short) 0x2a);
	/**
	 * _(* #,##0.00_);_(* (#,##0.00);_(* \"-\"??_);_(@_)
	 */
	public static final CellDataFormat CUSTOM11 = new CellDataFormat((short) 0x2b);
	/**
	 * _($* #,##0.00_);_($* (#,##0.00);_($* \"-\"??_);_(@_)
	 */
	public static final CellDataFormat CUSTOM12 = new CellDataFormat((short) 0x2c);
	/**
	 * 0%
	 */
	public static final CellDataFormat SIMPLEPERCENT = new CellDataFormat((short) 9);
	/**
	 * 0.00%
	 */
	public static final CellDataFormat DECIMALPERCENT = new CellDataFormat((short) 0xa);
	/**
	 * 0.00E+00
	 */
	public static final CellDataFormat EXPONENTIAL = new CellDataFormat((short) 0xb);
	/**
	 * # ?/?
	 */
	public static final CellDataFormat UNKNOWN1 = new CellDataFormat((short) 0xc);
	/**
	 * # ??/??
	 */
	public static final CellDataFormat UNKNOWN2 = new CellDataFormat((short) 0xd);
	/**
	 * m/d/yy
	 */
	public static final CellDataFormat DATE1 = new CellDataFormat((short) 0xe);
	/**
	 * d-mmm-yy
	 */
	public static final CellDataFormat DATE2 = new CellDataFormat((short) 0xf);
	/**
	 * d-mmm
	 */
	public static final CellDataFormat DATE3 = new CellDataFormat((short) 0x10);
	/**
	 * mmm-yy
	 */
	public static final CellDataFormat DATE4 = new CellDataFormat((short) 0x11);
	/**
	 * h:mm AM/PM
	 */
	public static final CellDataFormat DATE5 = new CellDataFormat((short) 0x12);
	/**
	 * h:mm:ss AM/PM
	 */
	public static final CellDataFormat DATE6 = new CellDataFormat((short) 0x13);
	/**
	 * h:mm
	 */
	public static final CellDataFormat DATE7 = new CellDataFormat((short) 0x14);
	/**
	 * h:mm:ss
	 */
	public static final CellDataFormat DATE8 = new CellDataFormat((short) 0x15);
	/**
	 * m/d/yy h:mm
	 */
	public static final CellDataFormat DATE9 = new CellDataFormat((short) 0x16);
	/**
	 * mm:ss
	 */
	public static final CellDataFormat DATE10 = new CellDataFormat((short) 0x2d);
	/**
	 * [h]:mm:ss
	 */
	public static final CellDataFormat DATE11 = new CellDataFormat((short) 0x2e);
	/**
	 * mm:ss.0
	 */
	public static final CellDataFormat DATE12 = new CellDataFormat((short) 0x2f);
	/**
	 * ##0.0E+0
	 */
	public static final CellDataFormat EXPONENTIAL2 = new CellDataFormat((short) 0x30);
	/**
	 * text or @
	 */
	public static final CellDataFormat TEXT = new CellDataFormat((short) 0x31);
	
	private short value;
	
	private CellDataFormat(short value)
	{
		this.value = value;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (!obj.getClass().equals(CellDataFormat.class))
			return false;
		CellDataFormat tmp = (CellDataFormat) obj;
		return (tmp.getIndex() == value);
	}
	
	public short getIndex()
	{
		return value;
	}
}
