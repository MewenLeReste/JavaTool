/**
 * A set of classes allowing us to manage Excel file by:
 * <br>Managing the file ({@link SpreadSheet})
 * <br>Managing his content ({@link SpreadSheetFormat}, {@link CellFormat} and {@link SpreadSheetPosition})
 */
package tool.ms.spreadsheet;