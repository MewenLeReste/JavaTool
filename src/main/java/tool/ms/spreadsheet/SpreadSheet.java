package tool.ms.spreadsheet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import tool.Time;
import tool.ms.TDocument;

/**
 * Creation and management of an Excel file
 */
@SuppressWarnings({"unused", "WeakerAccess", "EmptyCatchBlock"})
public class SpreadSheet extends TDocument<Sheet>
{
	private int freezeColumn = 0;
	private int freezeRow = 0;
	private Workbook workbook;
	private CellFormat defaultFormat = new CellFormat();
	private boolean copyFormatWhenAdd = true;

	/**
	 * Create a new spread sheet at the given path
	 * 
	 * @param name Path of the {@link SpreadSheet}
	 * @param forceCrush If true, delete the file
	 */
	public SpreadSheet(String name, boolean forceCrush)
	{
		super(name, forceCrush);
		try
		{
			if (file.exists())
			{
				if (file.getName().endsWith(".xls"))
					workbook = new HSSFWorkbook(new FileInputStream(file));
				else if (file.getName().endsWith(".xlsx"))
					workbook = new XSSFWorkbook(new FileInputStream(file));
				int n = 0;
				Sheet sheet = workbook.getSheetAt(n);
				while (sheet != null)
				{
					addPage(sheet);
					n++;
					try
					{
						sheet = workbook.getSheetAt(n);
					} catch (Exception e)
					{
						sheet = null;
					}
				}
				size = n;
				create = false;
			}
			else
			{
				if (file.getName().endsWith(".xls"))
					workbook = new HSSFWorkbook();
				else if (file.getName().endsWith(".xlsx"))
					workbook = new XSSFWorkbook();
			}
		} catch (Exception e) {}
	}
	
	/**
	 * Check if the {@link File} is a valid {@link SpreadSheet}
	 * 
	 * @param file {@link File} to check
	 * @return true if the {@link File} is a valid {@link SpreadSheet}
	 */
	public static boolean isExcel(File file)
	{
		if (!file.getName().endsWith(".xls") && !file.getName().endsWith(".xlsx"))
			return false;
		try
		{
			Workbook workbook;
			if (file.getName().endsWith(".xls"))
			{
				workbook = new HSSFWorkbook(new FileInputStream(file));
				workbook.close();
			}
			else if (file.getName().endsWith(".xlsx"))
			{
				workbook = new XSSFWorkbook(new FileInputStream(file));
				workbook.close();
			}
			return true;
		} catch (Exception e)
		{
			return false;
		}
	}
	
	//====================Inherited method====================\\
	public void close()
	{
		try
		{
			workbook.close();
		} catch (Exception e) {}
	}
	public void save()
	{
		try
		{
			workbook.write(new FileOutputStream(file));
		} catch (Exception e) {}
	}
	
	//====================Pages gestion====================\\
	/**
	 * Create a new page with the given name
	 * 
	 * @param name Name of the page
	 */
	public void newPage(String name)
	{
		try
		{
			addPage(workbook.createSheet(name));
			size++;
		} catch (Exception e) {}
	}
	/**
	 * Create a new page with the name "Sheet" following by the number of page
	 */
	public void newPage()
	{
		newPage("Sheet" + (size + 1));
	}
	/**
	 * Add the sheet with all his content, cell format and column/row size
	 * <br><b>Warning:
	 * <br>It will not copy the cells and sheet formats</b>
	 */
	public boolean add(Sheet page)
	{
		try
		{
			boolean added = false;
			String suffixe = "";
			String pageName = page.getSheetName();
			int n = 1;
			while (!added)
			{
				try
				{
					if (!addPage(workbook.createSheet(pageName + suffixe)))
						return false;
					added = true;
				} catch (IllegalArgumentException e)
				{
					int length = (pageName + suffixe).length();
					if (length > 31)
						pageName = pageName.substring(0, pageName.length() - (length - 31));
					else
					{
						suffixe = " (" + n + ")";
						n++;
					}
				}
			}
			Sheet newSheet = getLast();
			size++;
			int rowSize = page.getLastRowNum();
			for (int rowCount = 0; rowCount <= rowSize; rowCount++)
			{
				Row row = page.getRow(rowCount);
				if (row != null)
				{
					Row newRow = newSheet.createRow(rowCount);
					newRow.setHeight(row.getHeight());
					int columnSize = row.getLastCellNum();
					for (int columnCount = 0; columnCount < columnSize; columnCount++)
					{
						newSheet.setColumnWidth(columnCount, page.getColumnWidth(columnCount));
						Cell cell = row.getCell(columnCount);
						if (cell != null)
						{
							Cell newCell = newRow.createCell(columnCount);
							if (copyFormatWhenAdd)
							{
								try
								{
									if (cell.getClass().equals(newCell.getClass()))
									{
										CellStyle newStyle = workbook.createCellStyle();
										newStyle.cloneStyleFrom(cell.getCellStyle());
										newCell.setCellStyle(newStyle);
									}
								} catch (Exception e)
								{
									copyFormatWhenAdd = false;
								}
							}
							Object value = getCellContent(cell);
							if (value != null)
								setCellContent(newCell, value);
						}
					}
				}
			}
			return true;
		} catch (Exception e)
		{
			return false;
		}
	}
	/**
	 * Add the sheet with all his content, cell format and column/row size
	 * <br><b>Warning:
	 * <br>It will not copy the cells and sheet formats</b>
	 * 
	 * @param page Page
	 * @param format Specify if we copy the format or not
	 * @return true if the page has been copied
	 */
	public boolean add(Sheet page, boolean format)
	{
		boolean tmp = copyFormatWhenAdd;
		copyFormatWhenAdd = format;
		boolean ret = add(page);
		copyFormatWhenAdd = tmp;
		return ret;
	}
	/**
	 * Merge the cells in the given region of the given page
	 * 
	 * @param page Page where to merge
	 * @param firstRow firt row of the region to merge
	 * @param lastRow last row of the region to merge
	 * @param firstCol firt column of the region to merge
	 * @param lastCol last column of the region to merge
	 */
	public void mergeCells(int page, int firstRow, int lastRow, int firstCol, int lastCol)
	{
		if (page == SpreadSheetPosition.LAST)
			page = size - 1;
		else
			page -= 1;
		if (page > size)
			return ;
		try
		{
			get(page).addMergedRegion(new CellRangeAddress(firstRow, lastRow, firstCol, lastCol));
		} catch (Exception e) {}
	}
	/**
	 * Set the default cell value if no format to the given one
	 * 
	 * @param defaultFormat the format of the cells by default
	 */
	public void setDefaultCellFormat(CellFormat defaultFormat)
	{
		this.defaultFormat = defaultFormat;
	}
	/**
	 * Return a {@link SpreadSheetConditionalFormat} for the given page and range
	 * 
	 * @param page Page
	 * @param range Range of the cells for the conditional format
	 * @return A {@link SpreadSheetConditionalFormat}
	 */
	public SpreadSheetConditionalFormat getConditionalFormat(int page, SpreadSheetRange range)
	{
		if (page == SpreadSheetPosition.LAST)
			page = size - 1;
		else
			page -= 1;
		if (page > size)
			return null;
		if (suffix.equals("xlsx"))
			return new SpreadSheetConditionalFormat(((XSSFSheet)get(page)).getSheetConditionalFormatting(), range);
		else
			return null;
	}

	//====================Columns gestion====================\\
	/**
	 * Group columns
	 * 
	 * @param page Page
	 * @param first First column to group
	 * @param last Last column to group
	 */
	public void groupColumn(int page, int first, int last)
	{
		if (page == SpreadSheetPosition.LAST)
			page = size - 1;
		else
			page -= 1;
		if (page > size)
			return ;
		try
		{
			get(page).groupColumn(first, last);
		} catch (Exception e) {}
	}
	/**
	 * Freeze columnss at the left of the {@link SpreadSheet}
	 * 
	 * @param page Page
	 * @param col Last column to fix
	 */
	public void freezeColumn(int page, int col)
	{
		if (page == SpreadSheetPosition.LAST)
			page = size - 1;
		else
			page -= 1;
		if (page > size)
			return ;
		try
		{
			get(page).createFreezePane(col, freezeRow);
			freezeColumn = col;
		} catch (Exception e) {}
	}
	/**
	 * Specify the width of a column
	 * 
	 * @param page Page
	 * @param col Column
	 * @param width New width in pixels
	 */
	public void setColumnWidth(int page, int col, int width)
	{
		if (page == SpreadSheetPosition.LAST)
			page = size - 1;
		else
			page -= 1;
		if (page > size)
			return ;
		try
		{
			get(page).setColumnWidth(col, (((width + 1) / 7) * 256));
		} catch (Exception e) {}
	}

	//====================Rows gestion====================\\
	/**
	 * Group rows
	 * 
	 * @param page Page
	 * @param first First row to group
	 * @param last Last row to group
	 */
	public void groupRow(int page, int first, int last)
	{
		if (page == SpreadSheetPosition.LAST)
			page = size - 1;
		else
			page -= 1;
		if (page > size)
			return ;
		try
		{
			Sheet sheet = get(page);
			for (int row = first; row <= last; row++)
			{
				if (sheet.getRow(row) == null)
					sheet.createRow(row);
			}
			sheet.groupRow(first, last);
		} catch (Exception e) {}
	}
	/**
	 * Freeze rows at the top of the {@link SpreadSheet}
	 * 
	 * @param page Page
	 * @param row Last row to fix
	 */
	public void freezeRow(int page, int row)
	{
		if (page == SpreadSheetPosition.LAST)
			page = size - 1;
		else
			page -= 1;
		if (page > size)
			return ;
		try
		{
			Sheet sheet = get(page);
			for (int rowPos = 0; row <= rowPos; ++rowPos)
			{
				if (sheet.getRow(rowPos) == null)
					sheet.createRow(rowPos);
			}
			sheet.createFreezePane(freezeColumn, row);
			freezeRow = row;
		} catch (Exception e) {}
	}
	/**
	 * Specify the height of a row
	 * 
	 * @param page Page
	 * @param row Row
	 * @param height New height in pixels
	 */
	public void setRowHeight(int page, int row, int height)
	{
		if (page == SpreadSheetPosition.LAST)
			page = size - 1;
		else
			page -= 1;
		if (page > size)
			return ;
		try
		{
			Sheet sheet = get(page);
			if (sheet.getRow(row) == null)
				sheet.createRow(row);
			sheet.getRow(row).setHeightInPoints(((float)height * 72) / 96);
		} catch (Exception e) {}
	}

	//====================Content adding====================\\
	private boolean addImage(Sheet sheet, SpreadSheetImage image, SpreadSheetPosition pos)
	{
		if (!image.isCreated())
			return false;
		int index = workbook.addPicture(image.getBytes(), image.getType());
		ClientAnchor anchor = workbook.getCreationHelper().createClientAnchor();
		Drawing<?> drawing = sheet.createDrawingPatriarch();
		anchor.setCol1(pos.getColumn());
		if (sheet.getRow(pos.getRow()) == null)
			sheet.createRow(pos.getRow());
		anchor.setRow1(pos.getRow());
		Picture pict = drawing.createPicture(anchor, index);
		pict.resize();
		return true;
	}

	private void setCellContent(Cell cell, Object content)
	{
		if (content.getClass().equals(Percent.class))
		{
			double value = ((Percent) content).getPercent();
			cell.setCellValue(value);
		}
		else if (content.getClass().equals(Time.class))
		{
			try
			{
				Date value = ((Time) content).getDate();
                cell.setCellValue(value);
			} catch (Exception e) {}
		}
		else if (content.getClass().equals(Double.class))
			cell.setCellValue((double) content);
		else if (content.getClass().equals(Integer.class))
			cell.setCellValue((int) content);
		else if (content.getClass().equals(SpreadSheetFormula.class))
			cell.setCellFormula(((SpreadSheetFormula) content).getFormula());
		else
			cell.setCellValue(content.toString());
	}
	@SuppressWarnings("EmptyCatchBlock")
	private boolean addCell(Sheet sheet, Workbook workbook, CellFormat cellFormat, SpreadSheetPosition pos, Object content)
	{
		try
		{
			if (cellFormat == null)
				cellFormat = defaultFormat;
			if (content.getClass().equals(SpreadSheetImage.class))
				return addImage(sheet, (SpreadSheetImage) content, pos);
			Row row = sheet.getRow(pos.getRow());
			if (row == null)
				row = sheet.createRow(pos.getRow());
			Cell cell = row.createCell(pos.getColumn());
			setCellContent(cell, content);
			CellDataFormat dataFormat = cellFormat.getDataFormat();
			if (dataFormat == null)
			{
				if (content.getClass().equals(Percent.class))
					dataFormat = CellDataFormat.DECIMALPERCENT;
				else if (content.getClass().equals(Time.class))
					dataFormat = CellDataFormat.DATE4;
				else if (content.getClass().equals(Double.class))
					dataFormat = CellDataFormat.DECIMAL;
				else if (content.getClass().equals(Integer.class))
					dataFormat = CellDataFormat.NUMERICAL;
				else if (content.getClass().equals(SpreadSheetFormula.class))
					dataFormat = ((SpreadSheetFormula) content).getDataFormat();
			}
			try
			{
				cell.setCellStyle(cellFormat.formatCell(sheet, workbook, pos, dataFormat));
			} catch (Exception e) {}
			return true;
		} catch (Exception e)
		{
			return false;
		}
	}
	private boolean addWithoutFormat(SpreadSheetPosition pos, Object content)
	{
		int page = pos.getPage();
		if (page == SpreadSheetPosition.LAST)
			page = size - 1;
		else
			page -= 1;
		if (page > size)
			return false;
		Sheet sheet = get(page);
		return addCell(sheet, workbook, null, pos, content);
	}
	/**
	 * Add content in the {@link SpreadSheet}
	 * 
	 * @param format Format of the page
	 * @param pos {@link SpreadSheetPosition} of the content
	 * @param content The content to add
	 * @return true if the content has been added
	 */
	public boolean addContent(SpreadSheetFormat format, SpreadSheetPosition pos, Object content)
	{
		return addContent(format.formatCell(pos), pos, content);
	}
	/**
	 * Add content in the {@link SpreadSheet}
	 *
	 * @param format Format of the cell
	 * @param pos {@link SpreadSheetPosition} of the content
	 * @param content The content to add
	 * @return true if the content has been added
	 */
	public boolean addContent(CellFormat format, SpreadSheetPosition pos, Object content)
	{
		if (content == null)
			return false;
		if (format == null)
			return addWithoutFormat(pos, content);
		int page = pos.getPage();
		if (page == SpreadSheetPosition.LAST)
			page = size - 1;
		else
			page -= 1;
		if (page > size)
			return false;
		Sheet sheet = get(page);
		return addCell(sheet, workbook, format, pos, content);
	}
	/**
	 * Add content in the {@link SpreadSheet}
	 *
	 * @param format Format of the cell
	 * @param pos {@link SpreadSheetPosition} of the content
	 * @return true if the content has been added
	 */
	public boolean addContent(CellFormat format, SpreadSheetPosition pos)
	{
		return addContent(format, pos, "");
	}
	/**
	 * Add content in the {@link SpreadSheet}
	 * 
	 * @param format Format of the page
	 * @param range {@link SpreadSheetRange} corresponding to the cells where to place the content
	 * @param content The content to add
	 * @return true if the content has been added
	 */
	public boolean addContent(SpreadSheetFormat format, SpreadSheetRange range, Object content)
	{
		for (SpreadSheetPosition pos : range)
		{
			if (!addContent(format, pos, content))
				return false;
		}
		return true;
	}
	/**
	 * Add content in the {@link SpreadSheet}
	 * 
	 * @param pos {@link SpreadSheetPosition} of the content
	 * @param content The content to add
	 * @return true if the content has been added
	 */
    @SuppressWarnings("UnusedReturnValue")
	public boolean addContent(SpreadSheetPosition pos, Object content)
	{
		if (content == null)
			return false;
		return addWithoutFormat(pos, content);
	}
	/**
	 * Add content in the {@link SpreadSheet}
	 * 
	 * @param range {@link SpreadSheetRange} corresponding to the cells where to place the content
	 * @param content The content to add
	 * @return true if the content has been added
	 */
	public boolean addContent(SpreadSheetRange range, Object content)
	{
		return addContent(null, range, content);
	}

	//====================Content getter====================\\
    @SuppressWarnings("EmptyCatchBlock")
	private Object getCellContent(Cell cell)
	{
		boolean get = false;
		Object obj = null;
		try
		{
			String string = cell.getStringCellValue();
			if (string.isEmpty())
				return null;
			obj = string;
			get = true;
		} catch (Exception e) {}
		if (!get)
		{
			try
			{
				String format = cell.getCellStyle().getDataFormatString();
				double value = cell.getNumericCellValue();
				if (format.endsWith("%"))
				{
					obj = new Percent(value * 100);
					get = true;
				}
				else if (DateUtil.isCellDateFormatted(cell))
				{
					obj = new Time(cell.getDateCellValue());
					get = true;
				}
				else
				{
					obj = value;
					get = true;
				}
			} catch (Exception e) {}
		}
		if (!get)
		{
			try
			{
				obj = cell.getBooleanCellValue();
			} catch (Exception e) {}
		}
		try
		{
			String formula = cell.getCellFormula();
			if (formula.isEmpty())
				return null;
			String format = cell.getCellStyle().getDataFormatString();
			FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
			CellValue cellValue = evaluator.evaluate(cell);
			String cellType = cellValue.getCellTypeEnum().name();
			if (cellType.equals(CellType.NUMERIC.name()))
			{
				double value = cellValue.getNumberValue();
				if (format.endsWith("%"))
					return new SpreadSheetFormula(formula, CellDataFormat.DECIMALPERCENT, new Percent(value));
				else
					return new SpreadSheetFormula(formula, CellDataFormat.DECIMAL, value);
			}
			else if (cellType.equals(CellType.BOOLEAN.name()))
			{
				boolean bool = cellValue.getBooleanValue();
				return new SpreadSheetFormula(formula, CellDataFormat.NUMERICAL, bool);
			}
			else if (cellType.equals(CellType.STRING.name()))
			{
				String string = cellValue.getStringValue();
				return new SpreadSheetFormula(formula, CellDataFormat.TEXT, string);
			}
		} catch (Exception e) {}
		return obj;
	}
	/**
	 * Return the given cell content
	 * 
	 * @param pos {@link SpreadSheetPosition} of the cell
	 * @return Content of the given cell
	 */
	public Object getContent(SpreadSheetPosition pos)
	{
		int page = pos.getPage();
		if (page == SpreadSheetPosition.LAST)
			page = size - 1;
		else
			page -= 1;
		if (page > size)
			return null;
		try
		{
			Cell cell = get(page).getRow(pos.getRow()).getCell(pos.getColumn());
			if (cell == null)
				return null;
			return getCellContent(cell);
		} catch (Exception e)
		{
			return null;
		}
	}
	/**
	 * Return the page
	 * 
	 * @param page Page
	 * @return The page
	 */
	public Sheet getPage(int page)
	{
		if (page == SpreadSheetPosition.LAST)
			page = size - 1;
		else
			page -= 1;
		if (page > size)
			return null;
		return get(page);
	}
	/**
	 * Return the page name
	 * 
	 * @param page Page
	 * @return The name of the page
	 */
	public String getPageName(int page)
	{
		if (page == SpreadSheetPosition.LAST)
			page = size - 1;
		else
			page -= 1;
		if (page > size)
			return null;
		return get(page).getSheetName();
	}
}