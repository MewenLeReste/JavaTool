package tool.ms.spreadsheet;

import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;

/**
 * Class containing all the text alignment known by the {@link CellFormat}
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class CellAlignment
{
	public static final CellAlignment TOPCENTER = new CellAlignment(VerticalAlignment.TOP, HorizontalAlignment.CENTER);
	public static final CellAlignment TOPLEFT = new CellAlignment(VerticalAlignment.TOP, HorizontalAlignment.LEFT);
	public static final CellAlignment TOPRIGHT = new CellAlignment(VerticalAlignment.TOP, HorizontalAlignment.RIGHT);
	public static final CellAlignment CENTERCENTER = new CellAlignment(VerticalAlignment.CENTER, HorizontalAlignment.CENTER);
	public static final CellAlignment CENTERLEFT = new CellAlignment(VerticalAlignment.CENTER, HorizontalAlignment.LEFT);
	public static final CellAlignment CENTERRIGHT = new CellAlignment(VerticalAlignment.CENTER, HorizontalAlignment.RIGHT);
	public static final CellAlignment BOTTOMCENTER = new CellAlignment(VerticalAlignment.BOTTOM, HorizontalAlignment.CENTER);
	public static final CellAlignment BOTTOMLEFT = new CellAlignment(VerticalAlignment.BOTTOM, HorizontalAlignment.LEFT);
	public static final CellAlignment BOTTOMRIGHT = new CellAlignment(VerticalAlignment.BOTTOM, HorizontalAlignment.RIGHT);
	
	private VerticalAlignment vertical;
	private HorizontalAlignment horizontal;
	
	private CellAlignment(VerticalAlignment vertical, HorizontalAlignment horizontal)
	{
		this.vertical = vertical;
		this.horizontal = horizontal;
	}
	
	public VerticalAlignment getVerticalAlignment()
	{
		return vertical;
	}
	
	public HorizontalAlignment getHorizontalAlignment()
	{
		return horizontal;
	}
}
