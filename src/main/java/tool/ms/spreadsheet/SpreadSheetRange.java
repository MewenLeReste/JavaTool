package tool.ms.spreadsheet;

import java.util.Iterator;

import org.apache.poi.ss.util.CellRangeAddress;

/**
 * Range of cells in a {@link SpreadSheet}
 * <br><b>Warning</b> the first page is the page 1
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class SpreadSheetRange implements Iterable<SpreadSheetPosition>
{
	private int page;
	private int firstRow;
	private int lastRow;
	private int firstCol;
	private int lastCol;

	/**
	 * Create a new range of cells at the given page from/to the given cells
	 * 
	 * @param page Page of the range
	 * @param cell1 Address of one cell
	 * <br>Example: "A1"
	 * @param cell2 Address of another cell
	 * <br>Example: "B3"
	 */
	public SpreadSheetRange(int page, String cell1, String cell2)
	{
		this.page = page;
		SpreadSheetPosition pos1 = new SpreadSheetPosition(page, cell1);
		SpreadSheetPosition pos2 = new SpreadSheetPosition(page, cell2);
		setRange(pos1.getColumn(), pos1.getRow(), pos2.getColumn(), pos2.getRow());
	}
	/**
	 * Create a new range of cells at the given page from/to the given cells
	 * 
	 * @param page Page of the range
	 * @param cells Address of the cells
	 * <br>Example "A1:B3"
	 */
	public SpreadSheetRange(int page, String cells)
	{
		this.page = page;
		CellRangeAddress address = CellRangeAddress.valueOf(cells);
		firstCol = address.getFirstColumn();
		firstRow = address.getFirstRow();
		lastCol = address.getLastColumn();
		lastRow = address.getLastRow();
	}
	/**
	 * Create a new range of cells at the given page from/to the given cells
	 * 
	 * @param page Page of the range
	 * @param col1 Column of one cell
	 * @param row1 Row of one cell
	 * @param col2 Column of another cell
	 * @param row2 Row of another cell
	 */
	public SpreadSheetRange(int page, int col1, int row1, int col2, int row2)
	{
		this.page = page;
		setRange(col1, row1, col2, row2);
	}
	
	private void setRange(int col1, int row1, int col2, int row2)
	{
		if (col1 >= col2)
		{
			lastCol = col1;
			firstCol = col2;
		}
		else
		{
			lastCol = col2;
			firstCol = col1;
		}
		if (row1 >= row2)
		{
			lastRow = row1;
			firstRow = row2;
		}
		else
		{
			lastRow = row2;
			firstRow = row1;
		}
	}
	
	/**
	 * Return true if the given {@link SpreadSheetPosition} is the address of a cell inside the range
	 * 
	 * @param pos Position of a cell
	 * @return true if the cell is in the range
	 */
	public boolean isInRange(SpreadSheetPosition pos)
	{
		if (pos == null || pos.getPage() != page)
			return false;
		int col = pos.getColumn();
		int row = pos.getRow();
		return (col >= firstCol && col <= lastCol && row >= firstRow && row <= lastRow);
	}
	
	/**
	 * Return the page
	 * 
	 * @return The page
	 */
	public int getPage()
	{
		return page;
	}

	/**
	 * Return the first column
	 * 
	 * @return The first column
	 */
	public int getFirstColumn()
	{
		return firstCol;
	}
	
	/**
	 * Return the first row
	 * 
	 * @return The first row
	 */	
	public int getFirstRow()
	{
		return firstRow;
	}
	
	/**
	 * Return the last column
	 * 
	 * @return The last column
	 */
	public int getLastColumn()
	{
		return lastCol;
	}
	
	/**
	 * Return the last row
	 * 
	 * @return The last row
	 */
	public int getLastRow()
	{
		return lastRow;
	}
	
	CellRangeAddress getAddress()
	{
		return new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
	}

	/**
	 * Returns an iterator over elements of type {@link SpreadSheetPosition}.
	 * 
	 * @return An Iterator
	 */
	@Override
	public Iterator<SpreadSheetPosition> iterator()
	{
		return new SpreadSheetRangeIterator();
	}

	private class SpreadSheetRangeIterator implements Iterator<SpreadSheetPosition>
	{
		private SpreadSheetCursor cursor = null;
		
		@Override
		public boolean hasNext()
		{
			return (cursor == null || (isInRange(cursor) && !(cursor.getRow() == lastRow && cursor.getColumn() == lastCol)));
		}
	
		@Override
		public SpreadSheetPosition next()
		{
			if (cursor == null)
				cursor = new SpreadSheetCursor(page, firstCol, firstRow);
			else
			{
				cursor.nextColumn();
				if (cursor.getColumn() > lastCol)
					cursor.setCell(firstCol, cursor.getRow() + 1);
			}
			return cursor;
		}
	}
}