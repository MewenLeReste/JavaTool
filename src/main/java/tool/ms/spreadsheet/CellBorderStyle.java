package tool.ms.spreadsheet;

import org.apache.poi.ss.usermodel.BorderStyle;

/**
 * Class containing all the border format known by the {@link CellFormat}
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class CellBorderStyle
{
	public static final CellBorderStyle NONE = new CellBorderStyle(BorderStyle.NONE);
	public static final CellBorderStyle THICK = new CellBorderStyle(BorderStyle.THICK);
	public static final CellBorderStyle THIN = new CellBorderStyle(BorderStyle.THIN);
	public static final CellBorderStyle MEDIUM = new CellBorderStyle(BorderStyle.MEDIUM);
	public static final CellBorderStyle DASH_DOT = new CellBorderStyle(BorderStyle.DASH_DOT);
	public static final CellBorderStyle DASH_DOT_DOT = new CellBorderStyle(BorderStyle.DASH_DOT_DOT);
	public static final CellBorderStyle DASHED = new CellBorderStyle(BorderStyle.DASHED);
	public static final CellBorderStyle DOTTED = new CellBorderStyle(BorderStyle.DOTTED);
	public static final CellBorderStyle DOUBLE = new CellBorderStyle(BorderStyle.DOUBLE);
	public static final CellBorderStyle HAIR = new CellBorderStyle(BorderStyle.HAIR);
	public static final CellBorderStyle MEDIUM_DASH_DOT = new CellBorderStyle(BorderStyle.MEDIUM_DASH_DOT);
	public static final CellBorderStyle MEDIUM_DASH_DOT_DOT = new CellBorderStyle(BorderStyle.MEDIUM_DASH_DOT_DOT);
	public static final CellBorderStyle MEDIUM_DASHED = new CellBorderStyle(BorderStyle.MEDIUM_DASHED);
	public static final CellBorderStyle SLANTED_DASH_DOT = new CellBorderStyle(BorderStyle.SLANTED_DASH_DOT);

	private BorderStyle style;

	private CellBorderStyle(BorderStyle style)
	{
		this.style = style;
	}

	public BorderStyle getStyle()
	{
		return style;
	}
}