package tool.ms.spreadsheet;

/**
 * Position of the cursor in a {@link SpreadSheet}
 * <br><b>Warning</b> the first page is the page 1
 * Can be use in place of {@link SpreadSheetPosition}
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class SpreadSheetCursor extends SpreadSheetPosition
{
	public SpreadSheetCursor(int page, int column, int row)
	{
		super(page, column, row);
	}

	public SpreadSheetCursor(int page, String cell)
	{
		super(page, cell);
	}
	
	/**
	 * Put the cursor to the next row
	 */
	public void nextRow()
	{
		row++;
	}
	
	/**
	 * Put the cursor to the next column
	 */
	public void nextColumn()
	{
		column++;
	}
	
	/**
	 * Put the cursor to the previous row
	 */
	public void previousRow()
	{
		row--;
		if (row < 0)
			row = 0;
	}
	
	/**
	 * Put the cursor to the previous column
	 */
	public void previousColumn()
	{
		column--;
		if (column < 0)
			column = 0;
	}
	
	/**
	 * Put the cursor to the given cell
	 * 
	 * @param column Column of the cell
	 * @param row Row of the cell
	 */
	public void setCell(int column, int row)
	{
		this.column = column;
		this.row = row;
	}
	
	/**
	 * Put the cursor to the given cell
	 * 
	 * @param cell Excel reference of a cell
	 * <br>Example:"A1"
	 */
	public void setCell(String cell)
	{
		super.setCell(cell);
	}

	/**
	 * Put the cursor to the next page
	 */
	public void nextPage()
	{
		if (page > 0)
			page++;
	}

	/**
	 * Put the cursor to the previous page
	 */
	public void previousPage()
	{
		page--;
		if (page < 1)
			page = 1;
	}

	/**
	 * Put the cursor to the given page
	 * 
	 * @param page Page
	 */
	public void setPage(int page)
	{
		if (page > 0)
			this.page = page;
		else
			page = LAST;
	}
}
