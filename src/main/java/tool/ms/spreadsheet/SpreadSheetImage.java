package tool.ms.spreadsheet;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import tool.TImage;

/**
 * Image for the {@link SpreadSheet}
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class SpreadSheetImage extends TImage
{
	private int type;

	public SpreadSheetImage(TImage image)
	{
		super(image);
		setType();
	}
	
	public SpreadSheetImage(String path)
	{
		super(path);
		setType();
	}
	
	private void setType()
	{
		if (isCreated())
		{
			String suffix = getSuffix();
			if (suffix == null)
            {
                created = false;
                return;
            }
            switch (suffix)
            {
                case "bmp":
                    type = XSSFWorkbook.PICTURE_TYPE_DIB;
                case "emf": case "emz":
                    type = XSSFWorkbook.PICTURE_TYPE_EMF;
                case "jpg": case "jpeg":
                    type = XSSFWorkbook.PICTURE_TYPE_JPEG;
                case "pict": case "pct": case "pic":
                    type = XSSFWorkbook.PICTURE_TYPE_PICT;
                case "png":
                    type = XSSFWorkbook.PICTURE_TYPE_PNG;
                case "wmf": case "wmz":
                    type = XSSFWorkbook.PICTURE_TYPE_WMF;
                default:
                    created = false;
            }
		}
	}

	/**
	 * Return the type of the image
	 * 
	 * @return The type of the image
	 */
	public int getType()
	{
		return type;
	}
}
