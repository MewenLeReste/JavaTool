package tool.ms.spreadsheet;

import java.awt.Color;

import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFColor;

/**
 * The only known color format for {@link SpreadSheet}
 * <br>Use the rgb color only for the xlsx format otherwise the color will not be set
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class SpreadSheetColor extends XSSFColor
{
	public static final SpreadSheetColor AQUA = new SpreadSheetColor(IndexedColors.AQUA.getIndex());
	public static final SpreadSheetColor AUTOMATIC = new SpreadSheetColor(IndexedColors.AUTOMATIC.getIndex());
	public static final SpreadSheetColor BLACK = new SpreadSheetColor(IndexedColors.BLACK.getIndex());
	public static final SpreadSheetColor BLUE = new SpreadSheetColor(IndexedColors.BLUE.getIndex());
	public static final SpreadSheetColor BLUE_GREY = new SpreadSheetColor(IndexedColors.BLUE_GREY.getIndex());
	public static final SpreadSheetColor BRIGHT_GREEN = new SpreadSheetColor(IndexedColors.BRIGHT_GREEN.getIndex());
	public static final SpreadSheetColor BROWN = new SpreadSheetColor(IndexedColors.BROWN.getIndex());
	public static final SpreadSheetColor CORAL = new SpreadSheetColor(IndexedColors.CORAL.getIndex());
	public static final SpreadSheetColor CORNFLOWER_BLUE = new SpreadSheetColor(IndexedColors.CORNFLOWER_BLUE.getIndex());
	public static final SpreadSheetColor DARK_BLUE = new SpreadSheetColor(IndexedColors.DARK_BLUE.getIndex());
	public static final SpreadSheetColor DARK_GREEN = new SpreadSheetColor(IndexedColors.DARK_GREEN.getIndex());
	public static final SpreadSheetColor DARK_RED = new SpreadSheetColor(IndexedColors.DARK_RED.getIndex());
	public static final SpreadSheetColor DARK_TEAL = new SpreadSheetColor(IndexedColors.DARK_TEAL.getIndex());
	public static final SpreadSheetColor DARK_YELLOW = new SpreadSheetColor(IndexedColors.DARK_YELLOW.getIndex());
	public static final SpreadSheetColor GOLD = new SpreadSheetColor(IndexedColors.GOLD.getIndex());
	public static final SpreadSheetColor GREEN = new SpreadSheetColor(IndexedColors.GREEN.getIndex());
	public static final SpreadSheetColor GREY_25 = new SpreadSheetColor(IndexedColors.GREY_25_PERCENT.getIndex());
	public static final SpreadSheetColor GREY_40 = new SpreadSheetColor(IndexedColors.GREY_40_PERCENT.getIndex());
	public static final SpreadSheetColor GREY_50 = new SpreadSheetColor(IndexedColors.GREY_50_PERCENT.getIndex());
	public static final SpreadSheetColor GREY_80 = new SpreadSheetColor(IndexedColors.GREY_80_PERCENT.getIndex());
	public static final SpreadSheetColor INDIGO = new SpreadSheetColor(IndexedColors.INDIGO.getIndex());
	public static final SpreadSheetColor LAVENDER = new SpreadSheetColor(IndexedColors.LAVENDER.getIndex());
	public static final SpreadSheetColor LEMON_CHIFFON = new SpreadSheetColor(IndexedColors.LEMON_CHIFFON.getIndex());
	public static final SpreadSheetColor LIGHT_BLUE = new SpreadSheetColor(IndexedColors.LIGHT_BLUE.getIndex());
	public static final SpreadSheetColor LIGHT_CORNFLOWER_BLUE = new SpreadSheetColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
	public static final SpreadSheetColor LIGHT_GREEN = new SpreadSheetColor(IndexedColors.LIGHT_GREEN.getIndex());
	public static final SpreadSheetColor LIGHT_ORANGE = new SpreadSheetColor(IndexedColors.LIGHT_ORANGE.getIndex());
	public static final SpreadSheetColor LIGHT_TURQUOISE = new SpreadSheetColor(IndexedColors.LIGHT_TURQUOISE.getIndex());
	public static final SpreadSheetColor LIGHT_YELLOW = new SpreadSheetColor(IndexedColors.LIGHT_YELLOW.getIndex());
	public static final SpreadSheetColor LIME = new SpreadSheetColor(IndexedColors.LIME.getIndex());
	public static final SpreadSheetColor MAROON = new SpreadSheetColor(IndexedColors.MAROON.getIndex());
	public static final SpreadSheetColor OLIVE_GREEN = new SpreadSheetColor(IndexedColors.OLIVE_GREEN.getIndex());
	public static final SpreadSheetColor ORANGE = new SpreadSheetColor(IndexedColors.ORANGE.getIndex());
	public static final SpreadSheetColor ORCHID = new SpreadSheetColor(IndexedColors.ORCHID.getIndex());
	public static final SpreadSheetColor PALE_BLUE = new SpreadSheetColor(IndexedColors.PALE_BLUE.getIndex());
	public static final SpreadSheetColor PINK = new SpreadSheetColor(IndexedColors.PINK.getIndex());
	public static final SpreadSheetColor PLUM = new SpreadSheetColor(IndexedColors.PLUM.getIndex());
	public static final SpreadSheetColor RED = new SpreadSheetColor(IndexedColors.RED.getIndex());
	public static final SpreadSheetColor ROSE = new SpreadSheetColor(IndexedColors.ROSE.getIndex());
	public static final SpreadSheetColor ROYAL_BLUE = new SpreadSheetColor(IndexedColors.ROYAL_BLUE.getIndex());
	public static final SpreadSheetColor SEA_GREEN = new SpreadSheetColor(IndexedColors.SEA_GREEN.getIndex());
	public static final SpreadSheetColor SKY_BLUE = new SpreadSheetColor(IndexedColors.SKY_BLUE.getIndex());
	public static final SpreadSheetColor TAN = new SpreadSheetColor(IndexedColors.TAN.getIndex());
	public static final SpreadSheetColor TEAL = new SpreadSheetColor(IndexedColors.TEAL.getIndex());
	public static final SpreadSheetColor TURQUOISE = new SpreadSheetColor(IndexedColors.TURQUOISE.getIndex());
	public static final SpreadSheetColor VIOLET = new SpreadSheetColor(IndexedColors.VIOLET.getIndex());
	public static final SpreadSheetColor WHITE = new SpreadSheetColor(IndexedColors.WHITE.getIndex());
	public static final SpreadSheetColor YELLOW = new SpreadSheetColor(IndexedColors.YELLOW.getIndex());
	
	private short index;
	private boolean personalized;
	
	private SpreadSheetColor(int index)
	{
		this.index = (short) index;
		personalized = false;
	}
	
	public SpreadSheetColor(int r, int g, int b)
	{
		super(new Color(r, g, b));
		personalized = true;
	}
	
	public SpreadSheetColor(Color color)
	{
		super(color);
		personalized = true;
	}
	
	public boolean isPersonalized()
	{
		return personalized;
	}
	
	public short getIndex()
	{
		return index;
	}
}
