package tool.ms.spreadsheet;

import java.util.LinkedList;

import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFConditionalFormattingRule;
import org.apache.poi.xssf.usermodel.XSSFSheetConditionalFormatting;

/**
 * Conditional format of a page of a {@link SpreadSheet}
 */
@SuppressWarnings("unused")
public class SpreadSheetConditionalFormat
{
	public static final int XLS = 0;
	public static final int XLSX = 1;
	private XSSFSheetConditionalFormatting formatting;
	private XSSFConditionalFormattingRule rule;
	private LinkedList<CellRangeAddress> addresses = new LinkedList<>();
	private LinkedList<ConditionalFormat> formats = new LinkedList<>();

	SpreadSheetConditionalFormat(XSSFSheetConditionalFormatting formatting, SpreadSheetRange range)
	{
		rule = formatting.createConditionalFormattingRule("");
		this.formatting = formatting;
		addresses.add(range.getAddress());
	}
	
	/**
	 * Add a new range of cells for this conditional format
	 * 
	 * @param range Range of cells to add
	 */
	public void addRange(SpreadSheetRange range)
	{
		addresses.add(range.getAddress());
	}
	
	/**
	 * Set in the {@link SpreadSheet} the conditional format
	 */
	public void setConditionalFormat()
	{
		for (ConditionalFormat format : formats)
			format.setFormat();
		CellRangeAddress[] ranges = new CellRangeAddress[addresses.size()];
		for (int n = 0; n != addresses.size(); n++)
			ranges[n] = addresses.get(n);
		formatting.addConditionalFormatting(ranges, rule);
	}
	
	private ConditionalFormat add(ConditionalFormat conditionalFormat)
	{
		formats.add(conditionalFormat);
		return conditionalFormat;
	}

	/**
	 * Return an icon conditional formatting with the given set of icon
	 * 
	 * @param set Set of icon to use
	 * @return An icon conditional formatting
	 */
	public ConditionalIconFormat getConditionalIconFormat(ConditionalIconSet set)
	{
		return (ConditionalIconFormat) add(new ConditionalIconFormat(rule, set));
	}
}
