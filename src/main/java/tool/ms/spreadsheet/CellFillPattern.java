package tool.ms.spreadsheet;

import org.apache.poi.ss.usermodel.FillPatternType;

/**
 * Class containing all the background fill format known by the {@link CellFormat}
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class CellFillPattern
{
	public static final CellFillPattern ALT_BARS = new CellFillPattern(FillPatternType.ALT_BARS);
	public static final CellFillPattern BIG_SPOTS = new CellFillPattern(FillPatternType.BIG_SPOTS);
	public static final CellFillPattern BRICKS = new CellFillPattern(FillPatternType.BRICKS);
	public static final CellFillPattern DIAMONDS = new CellFillPattern(FillPatternType.DIAMONDS);
	public static final CellFillPattern FINE_DOTS = new CellFillPattern(FillPatternType.FINE_DOTS);
	public static final CellFillPattern LEAST_DOTS = new CellFillPattern(FillPatternType.LEAST_DOTS);
	public static final CellFillPattern LESS_DOTS = new CellFillPattern(FillPatternType.LESS_DOTS);
	public static final CellFillPattern NO_FILL = new CellFillPattern(FillPatternType.NO_FILL);
	public static final CellFillPattern SOLID = new CellFillPattern(FillPatternType.SOLID_FOREGROUND);
	public static final CellFillPattern SPARSE_DOTS = new CellFillPattern(FillPatternType.SPARSE_DOTS);
	public static final CellFillPattern SQUARES = new CellFillPattern(FillPatternType.SQUARES);
	public static final CellFillPattern THICK_BACKWARD_DIAG = new CellFillPattern(FillPatternType.THICK_BACKWARD_DIAG);
	public static final CellFillPattern THICK_FORWARD_DIAG = new CellFillPattern(FillPatternType.THICK_FORWARD_DIAG);
	public static final CellFillPattern THICK_HORZ_BANDS = new CellFillPattern(FillPatternType.THICK_HORZ_BANDS);
	public static final CellFillPattern THICK_VERT_BANDS = new CellFillPattern(FillPatternType.THICK_VERT_BANDS);
	public static final CellFillPattern THIN_BACKWARD_DIAG = new CellFillPattern(FillPatternType.THIN_BACKWARD_DIAG);
	public static final CellFillPattern THIN_FORWARD_DIAG = new CellFillPattern(FillPatternType.THIN_FORWARD_DIAG);
	public static final CellFillPattern THIN_HORZ_BANDS = new CellFillPattern(FillPatternType.THIN_HORZ_BANDS);
	public static final CellFillPattern THIN_VERT_BANDS = new CellFillPattern(FillPatternType.THIN_VERT_BANDS);
	
	private FillPatternType pattern;
	
	private CellFillPattern(FillPatternType pattern)
	{
		this.pattern = pattern;
	}

	public FillPatternType getPattern()
	{
		return pattern;
	}
}
