package tool.ms.spreadsheet;

import org.apache.poi.hssf.util.CellReference;

/**
 * Position of a cell in a {@link SpreadSheet}
 * <br><b>Warning</b> the first page is the page 1
 */
@SuppressWarnings("WeakerAccess")
public class SpreadSheetPosition
{
	protected int page;
	protected int row;
	protected int column;
	/**
	 * Point to the last page created on the spreadsheet
	 */
	public static final int LAST = -1;

	/**
	 * Create a {@link SpreadSheetPosition} with the given parameters
	 * 
	 * @param page Page
	 * @param column Column
	 * @param row Row
	 */
	public SpreadSheetPosition(int page, int column, int row)
	{
		if (page > 0)
			this.page = page;
		else
			this.page = LAST;
		this.column = column;
		this.row = row;
	}
	
	/**
	 * Create a {@link SpreadSheetPosition} with the given parameters
	 * 
	 * @param page Page
	 * @param cell Excel reference of a cell
	 * <br>Example:"A1"
	 */
	public SpreadSheetPosition(int page, String cell)
	{
		if (page > 0)
			this.page = page;
		else
			this.page = LAST;
		setCell(cell);
	}
	
	protected void setCell(String cell)
	{
		CellReference ref = new CellReference(cell);
		this.column = ref.getCol();
		this.row = ref.getRow();
	}

	public String toString()
	{
		return ("Page: " + page + " Column: " + column + " Row: " + row);
	}
	
	/**
	 * Return the page
	 * 
	 * @return The page
	 */
	public int getPage()
	{
		return page;
	}

	/**
	 * Return the row
	 * 
	 * @return The row
	 */
	public int getRow()
	{
		return row;
	}

	/**
	 * Return the column
	 * 
	 * @return The column
	 */
	public int getColumn()
	{
		return column;
	}
}
