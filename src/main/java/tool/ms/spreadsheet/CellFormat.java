package tool.ms.spreadsheet;

import java.awt.Color;
import java.util.Collection;
import java.util.LinkedList;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Class allowing to format a {@link SpreadSheet} cell with:
 * <br>- The cell color
 * <br>- The border color, position and style
 * <br>- The text alignment
 * <br>- The text font
 * <br>- The text format (bold, italic and underline)
 * <br>- The text color
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class CellFormat
{
	private LinkedList<CellStyle> format = new LinkedList<>();
	private LinkedList<CellDataFormat> dataFormatList = new LinkedList<>();

	//====================Position====================\\
	private LinkedList<SpreadSheetPosition> pos = new LinkedList<>();
	private LinkedList<SpreadSheetRange> range = new LinkedList<>();
	private boolean colAll;
	private boolean rowAll;
	private boolean borderAll;

	//====================Text format====================\\
	private String fontName;
	private int pointSize;
	private boolean bold;
	private boolean italic;
	private boolean strikeout;
	private byte underline;
	private SpreadSheetColor color;
	private CellAlignment alignment;
	private boolean wrap;

	//====================Cell format====================\\
	private SpreadSheetColor background;
	private CellFillPattern pattern;
	//=====Top border=====\\
	private boolean topBorder;
	private CellBorderStyle topBorderStyle;
	private SpreadSheetColor topBorderColor;
	//=====Bottom border=====\\
	private boolean bottomBorder;
	private CellBorderStyle bottomBorderStyle;
	private SpreadSheetColor bottomBorderColor;
	//=====Left border=====\\
	private boolean leftBorder;
	private CellBorderStyle leftBorderStyle;
	private SpreadSheetColor leftBorderColor;
	//=====Right border=====\\
	private boolean rightBorder;
	private CellBorderStyle rightBorderStyle;
	private SpreadSheetColor rightBorderColor;

	//=====================Cell content format====================\\
	private CellDataFormat dataFormat;

	public CellFormat()
	{
		init();
		colAll = false;
		rowAll = false;
		borderAll = false;
	}
	
	/**
	 * Initialize a basic format
	 * 
	 * @param pos List of cells positions
	 * @param column Specify is all the column is concerned by the format
	 * @param row Specify is all the row is concerned by the format
	 */
	public CellFormat(Collection<? extends SpreadSheetPosition> pos, boolean column, boolean row)
	{
		init();
		this.pos.addAll(pos);
		colAll = column;
		rowAll = row;
		borderAll = false;
	}
	
	/**
	 * Initialize a basic format
	 * 
	 * @param pos Cell position
	 * @param column Specify is all the column is concerned by the format
	 * @param row Specify is all the row is concerned by the format
	 */
	public CellFormat(SpreadSheetPosition pos, boolean column, boolean row)
	{
		init();
		this.pos.add(pos);
		colAll = column;
		rowAll = row;
		borderAll = false;
	}
	
	/**
	 * Initialize a basic format
	 * 
	 * @param range Cells position
	 * @param borderAll Specify if the border of the format apply on the range or the cells
	 * <br>If true all the cells in the range will have the border
	 */
	public CellFormat(SpreadSheetRange range, boolean borderAll)
	{
		init();
		this.range.add(range);
		this.borderAll = borderAll;
	}

	/**
	 * Initialize a basic format
	 * 
	 * @param range Cells position
	 * @param borderAll Specify if the border of the format apply on the range or the cells
	 * <br>If true all the cells in the range will have the border
	 */
	public CellFormat(Collection<? extends SpreadSheetRange> range, boolean borderAll)
	{
		init();
		this.range.addAll(range);
		this.borderAll = borderAll;
	}
	
	/**
	 * Add a list of position to the format
	 * 
	 * @param pos List of cells positions
	 */
	public void addPosition(Collection<? extends SpreadSheetPosition> pos)
	{
		this.pos.addAll(pos);
	}
	
	/**
	 * Add a position to the format
	 * 
	 * @param pos Cell position
	 */
	public void addPosition(SpreadSheetPosition pos)
	{
		this.pos.add(pos);
	}
	
	/**
	 * Add ranges of cells to the format
	 * 
	 * @param range List of cells range
	 */
	public void addRange(Collection<? extends SpreadSheetRange> range)
	{
		this.range.addAll(range);
	}
	
	/**
	 * Add a range of cells to the format
	 * 
	 * @param range Cells position
	 */
	public void addRange(SpreadSheetRange range)
	{
		this.range.add(range);
	}
	
	private void init()
	{
		fontName = "Calibri";
		pointSize = 11;
		bold = false;
		italic = false;
		strikeout = false;
		underline = Font.U_NONE;
		color = new SpreadSheetColor(Color.BLACK);
		alignment = CellAlignment.BOTTOMLEFT;
		wrap = false;
		background = new SpreadSheetColor(Color.WHITE);
		pattern = CellFillPattern.NO_FILL;
		topBorder = false;
		bottomBorder = false;
		leftBorder = false;
		rightBorder = false;
		dataFormat = null;
	}

	//====================Cell formatting====================\\
	boolean isFormatable(SpreadSheetPosition pos)
	{
		for (SpreadSheetRange rangePosition : range)
		{
			if (rangePosition.isInRange(pos))
				return true;
		}
		for (SpreadSheetPosition position : this.pos)
		{
			if ((position.getColumn() == pos.getColumn() && colAll) ||
				(position.getRow() == pos.getRow() && rowAll) ||
				(position.getColumn() == pos.getColumn() && position.getRow() == pos.getRow()))
				return true;
		}
		return false;
	}

	private CellStyle newFormat(Sheet sheet, Workbook workbook, SpreadSheetPosition pos)
	{
		try
		{
			SpreadSheetRange range = null;
			for (SpreadSheetRange rangePosition : this.range)
			{
				if (rangePosition.isInRange(pos))
					range = rangePosition;
			}
			CellStyle format = workbook.createCellStyle();
			if (dataFormat != null && !dataFormat.equals(CellDataFormat.EMPTY))
				format.setDataFormat(dataFormat.getIndex());
			format.setWrapText(wrap);
			if (!background.isPersonalized())
			{
				format.setFillForegroundColor(background.getIndex());
				format.setFillPattern(pattern.getPattern());
			}
			else if (background.isPersonalized() && workbook.getClass().equals(XSSFWorkbook.class))
			{
				((XSSFCellStyle) format).setFillForegroundColor(background);
				format.setFillPattern(pattern.getPattern());
			}
			format.setVerticalAlignment(alignment.getVerticalAlignment());
			format.setAlignment(alignment.getHorizontalAlignment());
			if (topBorder && (range == null || borderAll || pos.getRow() == range.getFirstRow()))
			{
				format.setBorderTop(topBorderStyle.getStyle());
				if (!topBorderColor.isPersonalized())
					format.setTopBorderColor(topBorderColor.getIndex());
				else if (topBorderColor.isPersonalized() && workbook.getClass().equals(XSSFWorkbook.class))
					((XSSFCellStyle) format).setTopBorderColor(topBorderColor);
			}
			if (bottomBorder && (range == null || borderAll || pos.getRow() == range.getLastRow()))
			{
				format.setBorderBottom(bottomBorderStyle.getStyle());
				if (!bottomBorderColor.isPersonalized())
					format.setBottomBorderColor(bottomBorderColor.getIndex());
				else if (bottomBorderColor.isPersonalized() && workbook.getClass().equals(XSSFWorkbook.class))
					((XSSFCellStyle) format).setBottomBorderColor(bottomBorderColor);
			}
			if (leftBorder && (range == null || borderAll || pos.getColumn() == range.getFirstColumn()))
			{
				format.setBorderLeft(leftBorderStyle.getStyle());
				if (!leftBorderColor.isPersonalized())
					format.setLeftBorderColor(leftBorderColor.getIndex());
				else if (leftBorderColor.isPersonalized() && workbook.getClass().equals(XSSFWorkbook.class))
					((XSSFCellStyle) format).setLeftBorderColor(leftBorderColor);
			}
			if (rightBorder && (range == null || borderAll || pos.getColumn() == range.getLastColumn()))
			{
				format.setBorderRight(rightBorderStyle.getStyle());
				if (!rightBorderColor.isPersonalized())
					format.setRightBorderColor(rightBorderColor.getIndex());
				else if (rightBorderColor.isPersonalized() && workbook.getClass().equals(XSSFWorkbook.class))
					((XSSFCellStyle) format).setRightBorderColor(rightBorderColor);
			}
			Font font = workbook.createFont();
			font.setBold(bold);
			font.setItalic(italic);
			if (!color.isPersonalized())
				font.setColor(color.getIndex());
			else if (color.isPersonalized() && workbook.getClass().equals(XSSFWorkbook.class))
			{
				((XSSFFont) font).setColor(color);
				format.setFillPattern(pattern.getPattern());
			}
			font.setStrikeout(strikeout);
			font.setFontName(fontName);
			font.setFontHeightInPoints((short)pointSize);
			font.setUnderline(underline);
			format.setFont(font);
			return format;
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	CellStyle formatCell(Sheet sheet, Workbook workbook, SpreadSheetPosition pos, CellDataFormat dataFormat)
	{
		boolean nullDataFormated = false;
		CellStyle style = null;
		if (this.dataFormat == null)
		{
			if (dataFormat != null)
				this.dataFormat = dataFormat;
			else
				this.dataFormat = CellDataFormat.EMPTY;
			nullDataFormated = true;
		}
		if (format.isEmpty() || (!borderAll && ! range.isEmpty()))
		{
			style = newFormat(sheet, workbook, pos);
			if (format.isEmpty())
			{
				format.add(style);
				dataFormatList.add(this.dataFormat);
			}
		}
		else
		{
			if (nullDataFormated)
			{
				boolean find = false;
				for (int n = 0; n != dataFormatList.size() && !find; n++)
				{
					if (dataFormatList.get(n).equals(this.dataFormat))
					{
						style = format.get(n);
						find = true;
					}
				}
				if (!find)
				{
					style = newFormat(sheet, workbook, pos);
					format.add(style);
					dataFormatList.add(this.dataFormat);
				}
			}
			else
				style = format.getFirst();
		}
		if (nullDataFormated)
			this.dataFormat = null;
		return style;
	}

	//====================Cell format====================\\
	/**
	 * Set the given border for all the border (top, bottom, left and right)
	 * 
	 * @param style Border style (thickness)
	 * @param color Color of the border
	 */
	public void setBorder(CellBorderStyle style, SpreadSheetColor color)
	{
		setTopBorder(style, color);
		setBottomBorder(style, color);
		setLeftBorder(style, color);
		setRightBorder(style, color);
	}
	/**
	 * Set the given border for the top border
	 * 
	 * @param style Border style (thickness)
	 * @param color Color of the border
	 */
	public void setTopBorder(CellBorderStyle style, SpreadSheetColor color)
	{
		topBorder = true;
		topBorderStyle = style;
		topBorderColor = color;
	}
	/**
	 * Set the given border for the bottom border
	 * 
	 * @param style Border style (thickness)
	 * @param color Color of the border
	 */
	public void setBottomBorder(CellBorderStyle style, SpreadSheetColor color)
	{
		bottomBorder = true;
		bottomBorderStyle = style;
		bottomBorderColor = color;
	}
	/**
	 * Set the given border for the left border
	 * 
	 * @param style Border style (thickness)
	 * @param color Color of the border
	 */
	public void setLeftBorder(CellBorderStyle style, SpreadSheetColor color)
	{
		leftBorder = true;
		leftBorderStyle = style;
		leftBorderColor = color;
	}
	/**
	 * Set the given border for the right border
	 * 
	 * @param style Border style (thickness)
	 * @param color Color of the border
	 */
	public void setRightBorder(CellBorderStyle style, SpreadSheetColor color)
	{
		rightBorder = true;
		rightBorderStyle = style;
		rightBorderColor = color;
	}
	/**
	 * Set the given border for all the border (top, bottom, left and right)
	 * 
	 * @param style Border style (thickness)
	 * @param color Color of the border
	 */
	public void setBorder(CellBorderStyle style, Color color)
	{
		setBorder(style, new SpreadSheetColor(color));
	}
	/**
	 * Set the given border for the top border
	 * 
	 * @param style Border style (thickness)
	 * @param color Color of the border
	 */
	public void setTopBorder(CellBorderStyle style, Color color)
	{
		setTopBorder(style, new SpreadSheetColor(color));
	}
	/**
	 * Set the given border for the bottom border
	 * 
	 * @param style Border style (thickness)
	 * @param color Color of the border
	 */
	public void setBottomBorder(CellBorderStyle style, Color color)
	{
		setBottomBorder(style, new SpreadSheetColor(color));
	}
	/**
	 * Set the given border for the left border
	 * 
	 * @param style Border style (thickness)
	 * @param color Color of the border
	 */
	public void setLeftBorder(CellBorderStyle style, Color color)
	{
		setLeftBorder(style, new SpreadSheetColor(color));
	}
	/**
	 * Set the given border for the right border
	 * 
	 * @param style Border style (thickness)
	 * @param color Color of the border
	 */
	public void setRightBorder(CellBorderStyle style, Color color)
	{
		setRightBorder(style, new SpreadSheetColor(color));
	}
	/**
	 * Set the background color with the given color and given fill pattern
	 * <br><b>WARNING :</b> Using a custom color format on xls will not work
	 * 
	 * @param pattern Fill pattern to use
	 * @param color color of the background
	 */
	public void setBackground(CellFillPattern pattern, SpreadSheetColor color)
	{
		this.pattern = pattern;
		this.background = color;
	}
	/**
	 * Set the background color with the given color and a solid fill pattern
	 * <br><b>WARNING :</b> Using a custom color format on xls will not work
	 * 
	 * @param color color of the background
	 */
	public void setBackground(SpreadSheetColor color)
	{
		setBackground(CellFillPattern.SOLID, color);
	}
	/**
	 * Set the background color with the given color and given fill pattern
	 * <br><b>WARNING :</b> Using a custom color format on xls will not work
	 * 
	 * @param pattern Fill pattern to use
	 * @param color color of the background
	 */
	public void setBackground(CellFillPattern pattern, Color color)
	{
		setBackground(pattern, new SpreadSheetColor(color));
	}
	/**
	 * Set the background color with the given color and a solid fill pattern
	 * <br><b>WARNING :</b> Using a custom color format on xls will not work
	 * 
	 * @param color color of the background
	 */
	public void setBackground(Color color)
	{
		setBackground(CellFillPattern.SOLID, new SpreadSheetColor(color));
	}

	//====================Text format====================\\
	public void setItalic(boolean italic)
	{
		this.italic = italic;
	}
	public void setStrikeout(boolean strikeout)
	{
		this.strikeout = strikeout;
	}
	public void setBold(boolean bold)
	{
		this.bold = bold;
	}
	/**
	 * Specify if the text had to be underline
	 * 
	 * @param underline Underline or not
	 * @param doubleLine Double underline or not
	 */
	public void setUnderline(boolean underline, boolean doubleLine)
	{
		if (underline)
		{
			if (doubleLine)
				this.underline = Font.U_DOUBLE;
			else
				this.underline = Font.U_SINGLE;
		}
		else
			this.underline = Font.U_NONE;
	}
	public void setFont(String font)
	{
		this.fontName = font;
	}
	public void setSize(int size)
	{
		pointSize = size;
	}
	/**
	 * Set the text color for the given one
	 * <br><b>WARNING :</b> Using a custom color format on xls will not work
	 * 
	 * @param color Color of the text
	 */
	public void setTextColor(SpreadSheetColor color)
	{
		this.color = color;
	}
	public void setAlignment(CellAlignment alignment)
	{
		this.alignment = alignment;
	}
	public void setWrap(boolean wrap)
	{
		this.wrap = wrap;
	}
	/**
	 * Specify the content format of the cell
	 * 
	 * @param format Format
	 */
	public void setFormat(CellDataFormat format)
	{
		dataFormat = format;
	}

	//====================Data format getter====================\\
	CellDataFormat getDataFormat()
	{
		return dataFormat;
	}
}