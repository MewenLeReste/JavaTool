package tool.ms.spreadsheet;

import org.apache.poi.ss.usermodel.IconMultiStateFormatting.IconSet;

/**
 * Set of 3 icons for the {@link ConditionalIconFormat}
 */
@SuppressWarnings("unused")
public final class ConditionalIconSet3 extends ConditionalIconSet
{
	public static final ConditionalIconSet3 ARROWS = new ConditionalIconSet3(IconSet.GREY_3_ARROWS);
	public static final ConditionalIconSet3 COLORED_ARROWS = new ConditionalIconSet3(IconSet.GYR_3_ARROW);
	public static final ConditionalIconSet3 COLORED_FLAG = new ConditionalIconSet3(IconSet.GYR_3_FLAGS);
	public static final ConditionalIconSet3 COLORED_SHAPES = new ConditionalIconSet3(IconSet.GYR_3_SHAPES);
	public static final ConditionalIconSet3 COLORED_SYMBOLS = new ConditionalIconSet3(IconSet.GYR_3_SYMBOLS);
	public static final ConditionalIconSet3 COLORED_SYMBOLS_CIRCLED = new ConditionalIconSet3(IconSet.GYR_3_SYMBOLS_CIRCLE);
	public static final ConditionalIconSet3 COLORED_TRAFFIC_LIGHT = new ConditionalIconSet3(IconSet.GYR_3_TRAFFIC_LIGHTS);
	public static final ConditionalIconSet3 COLORED_TRAFFIC_LIGHT_BOXED = new ConditionalIconSet3(IconSet.GYR_3_TRAFFIC_LIGHTS_BOX);

	private ConditionalIconSet3(IconSet set)
	{
		super(set);
	}
}
