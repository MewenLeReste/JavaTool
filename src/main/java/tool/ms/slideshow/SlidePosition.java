package tool.ms.slideshow;

/**
 * Position in a {@link SlidePresentation}
 */
@SuppressWarnings("WeakerAccess")
public class SlidePosition
{
	private int slide;
	private int placeholder;
	public static final int LAST = -1;

	/**
	 * Create a {@link SlidePosition} with the given parameters
	 * 
	 * @param slide Slide position
	 * @param placeholder Placeholder in the slide
	 */
	public SlidePosition(int slide, int placeholder)
	{
		if (slide > 0)
			this.slide = slide;
		else
			this.slide = LAST;
		this.placeholder = placeholder;
	}

	public String toString()
	{
		return ("Slide: " + slide + " Placeholder: " + placeholder);
	}
	
	/**
	 * Return the slide position
	 * 
	 * @return The slide position
	 */
	public int getSlide()
	{
		return slide;
	}

	/**
	 * Return the placeholder in the slide
	 * 
	 * @return The placeholder in the slide
	 */
	public int getPlaceholder()
	{
		return placeholder;
	}
}
