/**
 * A set of classes allowing us to manage PowerPoint file by:
 * <br>Managing the file ({@link SlidePresentation})
 * <br>Managing his content ({@link SlidePosition})
 * <br>
 * <br><b>WARNING: STILL IN DEVELOPMENT</b>
 */
package tool.ms.slideshow;
