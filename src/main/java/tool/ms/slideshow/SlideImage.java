package tool.ms.slideshow;

import org.apache.poi.sl.usermodel.PictureData;
import org.apache.poi.sl.usermodel.PictureData.PictureType;

import tool.TImage;

@SuppressWarnings("unused")
public class SlideImage extends TImage
{
	private PictureType type;

	public SlideImage(TImage image)
	{
		super(image);
		setType();
	}
	
	public SlideImage(String path)
	{
		super(path);
		setType();
	}

	private void setType()
	{
		if (isCreated())
		{
			String suffix = getSuffix();
			type = PictureData.PictureType.valueOf(suffix);
		}
	}
	
	/**
	 * Return the type of the image
	 * 
	 * @return The type of the image
	 */
	public PictureType getType()
	{
		return type;
	}
}
