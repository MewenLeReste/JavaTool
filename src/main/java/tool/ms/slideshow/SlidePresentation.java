package tool.ms.slideshow;

import java.awt.Dimension;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

import org.apache.poi.xslf.usermodel.SlideLayout;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFSlideLayout;
import org.apache.poi.xslf.usermodel.XSLFSlideMaster;
import org.apache.poi.xslf.usermodel.XSLFTextShape;

import tool.ms.TDocument;

/**
 * Creation and management of an PowerPoint file
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class SlidePresentation extends TDocument<XSLFSlide>
{
	private XMLSlideShow slideshow;
	
	/**
	 * Create a new slide show at the given path
	 * 
	 * @param name Path of the {@link SlidePresentation}
	 * @param forceCrush If true, delete the file
	 */
	public SlidePresentation(String name, boolean forceCrush)
	{
		super(name, forceCrush);
		try
		{
			if (file.exists())
			{
				slideshow = new XMLSlideShow(new FileInputStream(file));
				List<XSLFSlide> list = slideshow.getSlides();
				addAll(list);
				size = list.size();
				create = false;
			}
			else
				slideshow = new XMLSlideShow();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	//===================Parameters setters====================\\
	/**
	 * Set the slides size to the given parameter
	 * 
	 * @param width With of the slides
	 * @param height Height of the slides
	 */
	public void setSize(int width, int height)
	{
		slideshow.setPageSize(new Dimension(width, height));
	}
	/**
	 * Set the slides size to the given parameter
	 * 
	 * @param dimension Dimension of the slides
	 */
	public void setSize(Dimension dimension)
	{
		slideshow.setPageSize(dimension);
	}
	
	//===================Slides management====================\\
	/**
	 * Add the given content at the given position in the slide show
	 * 
	 * @param content Content to add
	 * @param position Position in the slide show
	 * @param clear Specify if we clear the placeholder or not
	 * @param newParagraph Specify if we create a new paragraph or not
	 * @return true if the content has been added
	 */
	public boolean addContent(String content, SlidePosition position, boolean clear, boolean newParagraph)
	{
		int page = position.getSlide();
		if (page == SlidePosition.LAST)
			page = size - 1;
		else
			page -= 1;
		if (page > size)
			return false;
		XSLFSlide slide = get(page);
		XSLFTextShape[] placeholders = slide.getPlaceholders();
		int index = position.getPlaceholder();
		if (index >= placeholders.length)
			return false;
		XSLFTextShape text = slide.getPlaceholder(index);
		if (clear)
			text.clearText();
		if (newParagraph)
			text.addNewTextParagraph().addNewTextRun().setText(content);
		else
			text.setText(content);
		return true;
	}
	/**
	 * Add a new slide in the presentation with the given layout and depending of the given master
	 * 
	 * @param layout Slide layout
	 * @param master Position of the SlideMaster
	 * @return true if the slide has been created
	 */
	public boolean addSlide(SlideLayout layout, int master)
	{
		List<XSLFSlideMaster> slideMasters = slideshow.getSlideMasters();
		if (master > slideMasters.size())
			return false;
		XSLFSlideMaster slideMaster = slideMasters.get(master);
		XSLFSlideLayout slideLayout = slideMaster.getLayout(layout);
		addPage(slideshow.createSlide(slideLayout));
		size++;
		return true;
	}
	/**
	 * Add a new slide in the presentation with the given layout
	 * 
	 * @param layout Slide layout
	 * @return true if the slide has been created
	 */
	public boolean addSlide(SlideLayout layout)
	{
		return addSlide(layout, 0);
	}
	/**
	 * Add a new slide in the presentation with the blank layout and depending of the given master
	 * 
	 * @param master Position of the SlideMaster
	 * @return true if the slide has been created
	 */
	public boolean addSlide(int master)
	{
		return addSlide(SlideLayout.BLANK, master);
	}
	/**
	 * Add a new slide in the presentation with the blank layout
	 * 
	 * @return true if the slide has been created
	 */
    @SuppressWarnings("UnusedReturnValue")
	public boolean addSlide()
	{
		return addSlide(SlideLayout.BLANK, 0);
	}
	/**
	 * Move the slide at the given position to the given new position
	 * 
	 * @param slideIndex Position of the slide to move
	 * @param newIndex New position of the slide
	 * @return true if the slide has been correctly move
	 */
	public boolean moveSlideTo(int slideIndex, int newIndex)
	{
		if (slideIndex > size || newIndex > size)
			return false;
		slideshow.setSlideOrder(get(slideIndex), newIndex);
		return true;
	}
	
	//====================Parameters getter====================\\
	/**
	 * Return the slides size
	 * 
	 * @return The slides size
	 */
	public Dimension getSize()
	{
		return slideshow.getPageSize();
	}

	//====================Inherited method====================\\
	public void close()
	{
		try
		{
			//slideshow.close();
		} catch (Exception e)
		{
		    e.printStackTrace();
		}
	}
	public void save()
	{
		try
		{
			slideshow.write(new FileOutputStream(file));
		} catch (Exception e)
		{
		    e.printStackTrace();
		}
	}
	public boolean add(XSLFSlide slide)
	{
		return false;
	}
}
