package tool.ms;

import java.io.Closeable;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Class representing a ms with pages
 *
 * @param <T> Class of the pages that compose the ms
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public abstract class TDocument<T> implements Closeable, Iterable<T>
{
	protected TFile file;
	protected String name;
	protected int size;
	protected boolean create;
	protected String suffix;
	private LinkedList<T> pages = new LinkedList<>();

	/**
	 * Create a new ms at the given path
	 * 
	 * @param name Path of the ms
	 * @param forceCrush If true, delete the file
	 */
	public TDocument(String name, boolean forceCrush)
	{
		file = new TFile(name);
		this.name = file.getName();
		suffix = this.name.substring(this.name.lastIndexOf(".") + 1);
		if (forceCrush)
			file.delete();
		create = true;
		size = 0;
	}

	@Override
	public String toString()
	{
		String created = "No";
		if (create)
			created = "Yes";
		return "Document [Path =" + name + ", Size =" + size + ", Create =" + created + "]";
	}

	/**
	 * Show if the ms has been created or not
	 * 
	 * @return true if the ms has been created
	 */
	public final boolean isNew()
	{
		return create;
	}
	/**
	 * Retun the pages number of the ms
	 * 
	 * @return The pages number of the ms
	 */
	public final int size()
	{
		return size;
	}
	/**
	 * Return the ms name
	 * 
	 * @return The ms name
	 */
	public final String getName()
	{
		return name;
	}
	/**
	 * Return the path of the ms
	 * 
	 * @return The path of the ms
	 */
	public final String getPath()
	{
		return file.getPath();
	}
	/**
	 * Return the file corresponding to the ms
	 * 
	 * @return The file corresponding to the ms
	 */
	public final TFile getFile()
	{
		return file;
	}
	/**
	 * Return the absolute path of the ms
	 * 
	 * @return The absolute path of the ms
	 */
	public final String getAbsolutePath()
	{
		return file.getAbsolutePath();
	}
	/**
	 * Delete the ms
	 */
	public final void delete()
	{
		try
		{
			close();
			file.delete();
		} catch (Exception e)
		{
		    e.printStackTrace();
		}
	}

	public final boolean isEmpty()
	{
		return pages.isEmpty();
	}

	@Override
	public Iterator<T> iterator()
	{
		return pages.iterator();
	}

	protected final boolean addPage(T e)
	{
		return pages.add(e);
	}

	@SuppressWarnings("UnusedReturnValue")
	protected final boolean addAll(Collection<? extends T> e)
	{
		return pages.addAll(e);
	}

	public final T get(int index)
	{
		return pages.get(index);
	}
	
	protected final T getLast()
	{
		return pages.getLast();
	}
	
	/**
	 * Add a page to the ms
	 * 
	 * @param page The page to add
	 * @return true if the given page has been added
	 */
	public abstract boolean add(T page);
	
	/**
	 * Close the ms
	 */
	public abstract void close();

	/**
	 * Save the ms
	 */
	public abstract void save();
}
